import React from 'react';
import { AppRegistry } from 'react-native';
import home from './src/index.js';

AppRegistry.registerComponent('chowmill', () => home);
