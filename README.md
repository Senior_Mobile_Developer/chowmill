# To **Pull** Project

> **Note:**

> - React-native needs to be install globally.
> - If not than follow link : <https://facebook.github.io/react-native/docs/getting-started.html>

- Go to Terminal

  - `$ git clone https://jacksolutions@bitbucket.org/Senior_Mobile_Developer/chowmill.git`
  - `$ cd chowmill`
  - `$ npm install`

--------------------------------------------------------------------------------

# To Run Project For **iOS**

> **Note:**

> - React-native needs to be install globally.
> - If not than follow link : <https://facebook.github.io/react-native/docs/getting-started.html>

If xcode and simulator proper configure

- Pull Code
- Go to Project Folder
- **In Terminal**

  - Install all node modules: `$ npm install && react-native link`
  - Run react native app for android: `$ react-native run-ios`

- It will run application in iOS Simulator

--------------------------------------------------------------------------------

# To Run Project For **Android**

> **Note:**

> - React-native needs to be install globally.
> - If not than follow link : <https://facebook.github.io/react-native/docs/getting-started.html>

If android studio and emulator proper configureâ€¦

- Pull Code
- Go to Project Folder
- **In Terminal**

  - Install all node modules: `$ npm install && react-native link`
  - Run react native app for android: `$ react-native run-android`

- **In studio**

  - open android studio
  - open existing project
  - open project_folder/android
  - clean project
  - run project in Emulator

## **For deubg .apk** that runs on out side development Enviorment

- **In Terminal**

  - Install all node modules: `$ npm install`
  - Run react native app for android: `$ react-native run-android`

- **In studio**

  - open android studio
  - open existing project
  - open project_folder/android
  - clean project
  - run project in Emulator

- If above steps done than close studio and emulator stops process in terminal

  - Goto Project Folder

- **In Terminal**

  - Setup bundle to build for debug `$ react-native bundle --dev false --platform android --entry-file index.android.js --bundle-output ./android/app/build/intermediates/assets/debug/index.android.bundle --assets-dest ./android/app/build/intermediates/res/merged/debug`
  - Go to Android Folder in Project `$ cd android`
  - Build for Debug `$ ./gradlew assembleDebug`

- once it is done

- go to Project_Folder/android/app/build/outputs/apk/**app-debug.apk** thereâ€™s ur apk and run in android device.

--------------------------------------------------------------------------------

# To Install **ipa** in **iOS**

> **Note:**

> - Device must be connected to MAC
> - Latest version of itunes must installed

If itunes and device proper configure

- Double click on **.ipa**

  - If it gives error like 'unauthorized developer'

    - than right click on **.ipa** file
    - click **open**
    - on popup also click **open**

- It will open itunes
- Select Your Device
- Go to **Apps**
- Click **Install**
- Click **Apply**
- It will install application on device.
