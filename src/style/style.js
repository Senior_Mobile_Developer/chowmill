import {Platform, Dimensions} from 'react-native';

/****************************/
/***** Pre Define Style *****/
/****************************/
var deviceWidth = Dimensions.get('window').width - 25;
var themeColor = '#D93F49';
var themeDisableColor = '#9E9E9E';
var themeLikedColor = '#01579B';
var themeContrastColor = '#FFFFFF';

//var backGroundColor = '#FCF9D8';
var backGroundColor = '#FFFFFF';

//var textColor = '#000000';
var textColor = '#FFFFFF';

var buttonTextSize = 14;
var smallTextSize = 13;
var mediumTextSize = 15;
var largeTextSize = 16;

export default {
  /* ######## General ######## */
  themeColor : {
    color: themeColor
  },
  themeDisableColor : {
    color: themeDisableColor
  },
  themeLikedColor : {
    color: themeLikedColor
  },
  themeContrastColor : {
    color: (Platform.OS === 'ios')
      ? themeColor
      : themeContrastColor
  },
  themeStatusBar : {
    backgroundColor: (Platform.OS === 'android')
      ? themeColor
      : themeContrastColor
  },
  themeLogoImage : {
    width: deviceWidth,
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'contain'
  },
  themeButton : {
    backgroundColor: themeColor
  },
  themeDisableButton : {
    backgroundColor: themeDisableColor
  },
  themeButtonText : {
    fontSize: buttonTextSize,
    color: textColor
  },
  themeText : {
    fontSize: smallTextSize
  },
  themeTextWithColor : {
    fontSize: smallTextSize,
    color: themeColor
  },
  themeTextWithDisableColor : {
    fontSize: smallTextSize,
    color: themeDisableColor
  },
  themeTextWithLikedColor : {
    fontSize: smallTextSize,
    color: themeLikedColor
  },
  themeBackButton : {
    color: '#000000'
  },
  themeHeaderTitle : {
    color: '#000000'
  },
  themeHeaderTitleBody : {
    flex: 2
  },
  columnView : {
    flexDirection: 'column'
  },
  themeBackgroundWhite : {
    backgroundColor: backGroundColor
  },
  /* ######## Splash Screen Component ######## */
  splashView : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
    alignItems: 'center',
    backgroundColor: backGroundColor
  },
  splashLoadingText : {
    color: themeColor,
    fontSize: 25
  },
  /* ######## Login Component ######## */
  loginView : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
    alignItems: 'center',
    backgroundColor: backGroundColor
  },
  loginInputView : {
    paddingBottom: 30
  },
  loginBottomText : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 20
  },
  loginInput : {
    marginBottom: 10,
    marginTop: 0
  },
  loginButton : {
    backgroundColor: themeColor,
    width: deviceWidth
  },
  /**** Login Component >>> SignIn ******/
  signInContentText : {
    color: '#4682B4',
    fontSize: smallTextSize
  },
  signInContentSingupText : {
    color: themeColor,
    fontSize: smallTextSize
  },
  signInImageView : {
    justifyContent: 'center',
    alignItems: 'center'
  },
  /**** Login Component >>> Signup ******/
  signUpContentText : {
    color: '#4682B4',
    fontSize: smallTextSize
  },
  signUpContentLoginText : {
    color: themeColor,
    fontSize: smallTextSize
  },
  signUpBottomText : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20
  },
  /**** Login Component >>> Forget Password ******/
  forgetPwStatusBar : {
    backgroundColor: themeContrastColor
  },
  forgetPwImageView : {
    justifyContent: 'center',
    alignItems: 'center'
  },
  /* ######## Home Component ######## */
  homeImage : {
    width: deviceWidth,
    height: deviceWidth,
    justifyContent: 'center',
    alignItems: 'center',
    resizeMode: 'stretch'
  },
  homeView : {
    backgroundColor: backGroundColor
  },
  homeImageCard : {
    justifyContent: 'center'
  },
  homeDetailCard : {
    justifyContent: 'space-around'
  },
  homePurchaseCard : {
    justifyContent: 'space-between'
  },
  homeBuyButton : {
    paddingRight: 20
  },
  homeMainTextCol : {
    justifyContent: 'center',
    alignItems: 'center'
  },
  homeMainText : {
    fontSize: largeTextSize,
    color: themeColor,
    fontWeight: 'bold'
  },
  /* ######## Home Component  >>>> Purchase Item ######## */
  purchaseView : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
    alignItems: 'center',
    backgroundColor: backGroundColor
  },
  purchaseStatusBar : {
    backgroundColor: themeContrastColor
  },
  purchaseCardItem : {
    justifyContent: 'space-between'
  },
  purchaseLabelItem : {
    justifyContent: 'space-around'
  },
  purchaseTextInput : {
    height: 40,
    width: 30,
    borderColor: 'gray',
    borderWidth: 1,
    textAlign: 'center'
  },
  purchaseContentText : {
    fontSize: smallTextSize
  },
  purchaseContentTextPaddig : {
    paddingTop: 10,
    fontSize: smallTextSize
  },
  purchaseAddressCard : {
    width: deviceWidth - 1
  },
  purchaseAddressContent : {
    justifyContent: 'space-between',
    flexDirection: 'column'
  },
  purchaseHeadContent : {
    justifyContent: 'center'
  },
  purchaseHeadContentDelivery : {
    justifyContent: 'space-between'
  },
  purchaseHeadContentText : {
    color: themeColor,
    fontSize: smallTextSize
  },
  purchaseEstimateText : {
    color: '#4682B4',
    fontSize: smallTextSize
  },
  purchaseModalView : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
    alignItems: 'center',
    backgroundColor: backGroundColor
  },
  /* ######## Home Component  >>>> Purchase Confirmation ######## */
  purchaseConfirmView : {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 10,
    alignItems: 'center',
    backgroundColor: backGroundColor
  },
  purchaseConfirmStatusBar : {
    backgroundColor: themeContrastColor
  },
  purchaseConfirmCardItem : {
    justifyContent: 'space-between'
  },
  purchaseConfirmLabelItem : {
    justifyContent: 'space-around'
  },
  purchaseConfirmContentText : {
    fontSize: smallTextSize
  },
  purchaseConfirmHeadContent : {
    justifyContent: 'center'
  },
  purchaseConfirmHeadContentText : {
    color: themeColor,
    fontSize: smallTextSize
  },
  purchaseConfirmEstimateText : {
    color: '#4682B4',
    fontSize: smallTextSize
  },
  /* ######## Home Component  >>>> SideMenu Pages ######## */
  sideMenuPageStatusBar : {
    backgroundColor: themeContrastColor
  },
  sideMenuPageTitle : {
    color: '#000000'
  },
  sideMenuPageContentText : {
    fontSize: smallTextSize
  },
  sideMenuPageCardItem : {
    justifyContent: 'space-between'
  },
  /* ######## Home Component  >>>> Menu Manager ######## */
  menuManagerInput : {
    height: 40,
    width: 40,
    borderColor: 'gray',
    borderWidth: 1,
    textAlign: 'center'
  },
  /* ######## Home Component  >>>> Purchase Profile ######## */
  purchaseCardCotainer : {
    flex: 1,
    justifyContent: 'center'
  },
  purchaseProfileContentColor : {
    color: '#808080',
    fontSize: smallTextSize
  },
  purchaseProfileHeaderColor : {
    color: themeColor,
    fontSize: smallTextSize
  },
  purchaseProfileCloseColor : {
    color: '#4682B4'
  },
  purchaseProfilePageCardItem : {
    justifyContent: 'space-between',
    padding: 10
  },
  /* ######## Home Component  >>>> Orders ######## */
  ordersHeadCardItem : {
    justifyContent: 'center'
  },
  ordersHeadText : {
    paddingLeft: 10,
    color: themeColor,
    fontSize: smallTextSize
  },
  orderIndex : {
    color: '#4682B4',
    fontSize: largeTextSize,
    fontWeight: 'bold'
  },
  orderIndexItemContent : {
    color: themeColor,
    fontSize: mediumTextSize
  },
  orderItemContent : {
    color: themeColor,
    fontSize: smallTextSize
  },
  orderItemHead : {
    fontSize: smallTextSize
  },
  orderItemLoop : {
    padding: 10
  },
  orderRecieptLoop : {
    height: 100,
    padding: 10
  },
  orderRecieptView : {
    justifyContent: 'space-between'
  },
  orderRecieptContent : {
    color: '#808080',
    fontSize: smallTextSize
  },
  orderItemLoopPadding : {
    paddingTop: 4
  },
  orderPadTopBottom : {
    paddingTop: 10,
    paddingBottom: 10
  },
  orderPadTopBottomLeft : {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 5
  },
  /* ######## SideMenu Component ######## */
  sideMenuView : {
    backgroundColor: backGroundColor
  }
};
