var local = false;
var url = '';
var socketurl = '';
if (local) {
  // url = 'http://192.168.0.102:3000';
  // socketurl = 'http://192.168.0.102:3001';

  url = 'http://localhost:3000';
  socketurl = 'http://localhost:3001';
} else {
  url = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3000';
  socketurl = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3001';

  //url = 'https://be.chowmill.com';
}

let localCI = 'client_id=ca_AZhU6DsUWGuihgh2w5WnGZrufCiqKNpA&scope=read_write&state=';
let serverCID = 'client_id=ca_AE1uBr2nGeAv7UUJ2xN9tz15aMFAXpfN&scope=read_write&state=';
let serverCIP = 'client_id=ca_AE1unEG5ZohlAsRZxfUCdnMaTvwFsDQd&scope=read_write&state=';

let prefill = '&stripe_user[url]=https://chowmill.com'
+ '&stripe_user[business_type]=sole_prop'
+ '&stripe_user[product_description]=FoodDelivery'
+ '&stripe_user[business_name]=CHOWMILL INC.'
+ '&stripe_user[email]=';

var stripeConnectUrl = 'https://connect.stripe.com/oauth/authorize?response_type=code&'
+ serverCIP + 'bhavanpatel@jacksolutions.biz' + prefill + 'bhavanpatel@jacksolutions.biz';
export default {
  baseURL: url,
  baseSocketURL: socketurl,
  stripeConnect: stripeConnectUrl,
};
