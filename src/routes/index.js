import React, {Component} from 'react';
import {AsyncStorage} from 'react-native';
import {Scene, Router} from 'react-native-router-flux';

//Splash
import Splash from '../splashscreen_component';

//Login
import Signin from '../login_component/Signin';
import Signup from '../login_component/Signup';
import ForgetPw from '../login_component/ForgetPw';
import ResetPw from '../login_component/ResetPw';

//Home
import HomeScreen from '../home_component/HomeScreen';
import SideNavBar from '../home_component/sidemenu_component';
import PurchaseScreen from '../home_component/PurchaseScreen';
import PurchaseConfirmationScreen from '../home_component/PurchaseConfirmationScreen';
import PaymentProfile from '../home_component/PaymentProfile';
import PaymentProfileSeller from '../home_component/PaymentProfileSeller';
import MenuManager from '../home_component/MenuManager';
import Orders from '../home_component/Orders';
import TermsCondition from '../home_component/TermsCondition';
import PrivacyPolicy from '../home_component/PrivacyPolicy';
import AboutUs from '../home_component/AboutUs';
import ContactUs from '../home_component/ContactUs';
import EditAddress from '../home_component/EditAddress';

var KEY = 'isLIn';

export default class AppRoutes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn: false,
      isLoading: true
    };
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState() {
    try {
      let value = await AsyncStorage.getItem(KEY);
      if (value !== null && value === 'true') {
        this.setState({isLoggedIn: true, isLoading: false});
      } else {
        this.setState({isLoggedIn: false, isLoading: false});
      }
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _loginscreen() {
    return (
      <Scene key="root" hideNavBar hideTabBar>
        <Scene key="Signin" component={Signin} title="Signin" initial={true} panHandlers={null}/>
        <Scene key="Home" component={HomeScreen} title="Home" panHandlers={null}/>

        <Scene key="Signup" component={Signup} title="Signup" panHandlers={null}/>
        <Scene key="ForgetPw" component={ForgetPw} title="ForgetPw" panHandlers={null}/>
        <Scene key="ResetPw" component={ResetPw} title="ResetPw" panHandlers={null}/>
        <Scene key="Purchase" component={PurchaseScreen} title="Purchase" panHandlers={null}/>
        <Scene key="PurchaseConfirm" component={PurchaseConfirmationScreen} title="PurchaseConfirm" panHandlers={null}/>
        <Scene key="MenuManager" component={MenuManager} title="MenuManager" panHandlers={null}/>
        <Scene key="Orders" component={Orders} title="Orders" panHandlers={null}/>
        <Scene key="PaymentProfile" component={PaymentProfile} title="PaymentProfile" panHandlers={null}/>
        <Scene key="PaymentProfileSeller" component={PaymentProfileSeller} title="PaymentProfileSeller" panHandlers={null}/>
        <Scene key="EditAddress" component={EditAddress} title="EditAddress" panHandlers={null}/>
        <Scene key="TermsCondition" component={TermsCondition} title="TermsCondition" panHandlers={null}/>
        <Scene key="PrivacyPolicy" component={PrivacyPolicy} title="PrivacyPolicy" panHandlers={null}/>
        <Scene key="AboutUs" component={AboutUs} title="AboutUs" panHandlers={null}/>
        <Scene key="ContactUs" component={ContactUs} title="ContactUs" panHandlers={null}/>
      </Scene>
    );
  }

  _homescreen() {
    return (
      <Scene key="root" hideNavBar hideTabBar>
        <Scene key="Home" component={HomeScreen} title="Home" initial={true} panHandlers={null}/>
        <Scene key="Signin" component={Signin} title="Signin" panHandlers={null}/>

        <Scene key="Signup" component={Signup} title="Signup" panHandlers={null}/>
        <Scene key="ForgetPw" component={ForgetPw} title="ForgetPw" panHandlers={null}/>
        <Scene key="ResetPw" component={ResetPw} title="ResetPw" panHandlers={null}/>
        <Scene key="Purchase" component={PurchaseScreen} title="Purchase" panHandlers={null}/>
        <Scene key="PurchaseConfirm" component={PurchaseConfirmationScreen} title="PurchaseConfirm" panHandlers={null}/>
        <Scene key="MenuManager" component={MenuManager} title="MenuManager" panHandlers={null}/>
        <Scene key="Orders" component={Orders} title="Orders" panHandlers={null}/>
        <Scene key="PaymentProfile" component={PaymentProfile} title="PaymentProfile" panHandlers={null}/>
        <Scene key="PaymentProfileSeller" component={PaymentProfileSeller} title="PaymentProfileSeller" panHandlers={null}/>
        <Scene key="EditAddress" component={EditAddress} title="EditAddress" panHandlers={null}/>
        <Scene key="TermsCondition" component={TermsCondition} title="TermsCondition" panHandlers={null}/>
        <Scene key="PrivacyPolicy" component={PrivacyPolicy} title="PrivacyPolicy" panHandlers={null}/>
        <Scene key="AboutUs" component={AboutUs} title="AboutUs" panHandlers={null}/>
        <Scene key="ContactUs" component={ContactUs} title="ContactUs" panHandlers={null}/>
      </Scene>
    );
  }

  render() {
    if (this.state.isLoading === true) {
      return (<Splash/>);
    } else {
      if (this.state.isLoggedIn === true) {
        return (
          <Router>{this._homescreen()}</Router>
        );
      } else {
        return (
          <Router>{this._loginscreen()}</Router>
        );
      }
    }
  }
}
