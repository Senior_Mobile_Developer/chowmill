import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  AsyncStorage,
  TouchableWithoutFeedback,
  Keyboard,
  AppState,
  Platform
} from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text
} from 'native-base';
import styles from '../style/style';
import {Actions} from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import axios from 'axios';
import Config from '../config/Config';

import PushController from '../config/PushController';
import PushNotification from 'react-native-push-notification';

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

const logoImg = require('../assets/logo1.png');
var KEY = 'isLIn';
var TOKEN = 'token';
var UTYPE = 'isSeller';
var UADD = 'address';
var STRIPE = 'stripeConnected';
var MAIL = 'email';
let GEODATA = null;

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      // seconds: 5
    };
    // this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }
  // componentDidMount() {
  //   AppState.addEventListener('change', this.handleAppStateChange);
  // }
  //
  // componentWillUnmount() {
  //   AppState.removeEventListener('change', this.handleAppStateChange);
  // }
  //
  // handleAppStateChange(appState) {
  //   if (appState === 'background') {
  //     let date = new Date(Date.now() + (this.state.seconds * 1000));
  //     console.log(date);
  //     PushNotification.localNotificationSchedule({
  //       message: "Hello Bhavan", // (required)
  //       date: date // in 5 secs
  //     })
  //   }
  // }

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0
    });
  }

  _validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  _logIn() {
    if (this.state.email === null || this.state.email === '') {
      this._showToast('Please Enter Email');
    } else if (this.state.password === null || this.state.password === '') {
      this._showToast('Please Enter Password');
    } else if (!this._validateEmail(this.state.email)) {
      this._showToast('Please Enter Valid Email address');
    } else {
      axios.post('/userLogin', {
        mail: this.state.email,
        password: this.state.password
      }).then((response) => {
        console.info('Login:', response.data);
        if (response.data.code === 200) {
          try {
            AsyncStorage.removeItem(KEY);
            AsyncStorage.removeItem(TOKEN);
            AsyncStorage.removeItem(UTYPE);
            AsyncStorage.removeItem(UADD);
            AsyncStorage.removeItem(STRIPE);
            AsyncStorage.removeItem(MAIL);
            AsyncStorage.setItem(KEY, 'true');
            AsyncStorage.setItem(TOKEN, response.data.authToken);
            AsyncStorage.setItem(UTYPE, JSON.stringify(response.data.data.isSeller));
            AsyncStorage.setItem(STRIPE, JSON.stringify(response.data.data.stripeConnected));
            AsyncStorage.setItem(UADD, response.data.data.address);
            AsyncStorage.setItem(MAIL, response.data.data.email);
            Actions.Home();
          } catch (error) {
            console.error('Error:AsyncStorage:', error.message);
          }
        }

        if (response.data.code === 403) {
          this._showToast(response.data.message);
        }
      }).catch(function(error) {
        console.log(error);
      });
    }
  };

  _forgetPassword() {
    Actions.ForgetPw();
  }

  _signUp() {
    Actions.Signup();
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.loginView}>
          <View style={styles.loginInputView}>
            <Image style={styles.themeLogoImage} source={logoImg}/>
            <Form>
              <Item floatingLabel style={styles.loginInput}>
                <Label style={styles.themeText}>Email</Label>
                <Input keyboardType='email-address' style={styles.themeText} autoCapitalize='none' onChangeText={(value) => {
                  this.setState({email: value.toLowerCase().trim()});
                }}/>
              </Item>
              <Item floatingLabel style={styles.loginInput}>
                <Label style={styles.themeText}>Password</Label>
                <Input secureTextEntry={true} style={styles.themeText} onChangeText={(value) => {
                  this.setState({password: value.trim()});
                }}/>
              </Item>
            </Form>
          </View>
          <View>
            <Button block onPress={this._logIn.bind(this)} style={styles.loginButton}>
              <Text style={styles.themeButtonText}>Login</Text>
            </Button>
          </View>
          <View style={styles.loginBottomText}>
            <Label style={styles.signInContentText} onPress={this._forgetPassword.bind(this)}>Forgot Password?</Label>
            <Label style={styles.signInContentText} onPress={this._signUp.bind(this)}>
              Don't have an account?
              <Label style={styles.signInContentSingupText}>
                Sign Up</Label>
            </Label>
          </View>
          <PushController/>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

export default Signin;
