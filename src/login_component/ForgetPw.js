import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator,
TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Container, Content, Form, Item,
  Input, Label, Button, Text,
  Header, Left, Icon, Body, Title, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import styles from '../style/style';
import axios from 'axios';
import Config from '../config/Config';

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

const logoImg = require('../assets/logo1.png');

class ForgetPw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      email: null,
    };
  }

  _goBack() {
    Actions.pop();
  }

  _showLoader() {
    this.setState({ animating: true });
  }

  _hideLoader() {
    this.setState({ animating: false });
  }

  _showToast(msg) {
    Toast.show(msg, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
  }

  _validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  _sendEmail() {
    if (this.state.email === null || this.state.email === '') {
      this._showToast('Please Enter Email');
    } else if (!this._validateEmail(this.state.email)) {
      this._showToast('Please Enter Valid Email address');
    } else {
      this._showLoader();
      axios.post('/userForgotPassword', {
        mail: this.state.email,
      })
      .then((response) => {
        if (response.data.code === 200) {
          this._hideLoader();
          Toast.show('Please Check Your Email', {
              duration: Toast.durations.SHORT,
              position: Toast.positions.BOTTOM,
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
              onHidden: () => {
                    Actions.ResetPw();
                  },
            });
        }

        if (response.data.code === 403) {
          this._hideLoader();
          this._showToast(response.data.message);
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  }

  _renderButton() {
      if (this.state.animating === true) {
        return (
          <Button block disabled>
            <Text style={styles.themeButtonText}>Send Email</Text>
          </Button>
        )
      } else {
        return (
          <Button block style={styles.loginButton} onPress={this._sendEmail.bind(this)}>
            <Text style={styles.themeButtonText}>Send Email</Text>
          </Button>
        )
      }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
            <View style={styles.loginView}>
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.forgetPwStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                        <Button transparent  onPress={this._goBack.bind(this)}>
                            <Icon name='arrow-back' style={styles.themeBackButton}/>
                        </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.themeHeaderTitle}>Reset Password</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <View style={styles.forgetPwImageView}>
                      <Image style={styles.themeLogoImage} source={logoImg}/>
                    </View>
                    <View style={styles.loginInputView}>
                      <Form>
                        <Item floatingLabel style={styles.loginInput}>
                            <Label style={styles.themeText}>Email</Label>
                            <Input style={styles.themeText}
                              autoCapitalize='none'
                              onChangeText={(value) =>
                                {
                                  this.setState({
                                    email: value.toLowerCase().trim(),
                                  });
                                }
                              }/>
                        </Item>
                      </Form>
                    </View>
                    <ActivityIndicator
                      animating= {this.state.animating}
                      color='#D93F49'
                      size='large'
                    />
                    {this._renderButton()}
                </Content>
              </Container>
          </View>
        </TouchableWithoutFeedback>
          );
  }
}

export default ForgetPw;
