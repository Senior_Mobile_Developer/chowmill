import React, { Component } from 'react';
import { StyleSheet, View, Image, ActivityIndicator,
TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Container, Content, Form, Item,
  Input, Label, Button, Text,
  Header, Left, Icon, Body, Title, Right } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import styles from '../style/style';
import axios from 'axios';
import Config from '../config/Config';

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

const logoImg = require('../assets/logo1.png');

class ResetPw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: false,
      code: null,
      password: null,
    };
  }

  _goBack() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
  }

  _showLoader() {
    this.setState({ animating: true });
  }

  _hideLoader() {
    this.setState({ animating: false });
  }

  _sendEmail() {
    if (this.state.code === null || this.state.code === '') {
      this._showToast('Please Enter Code');
    } else if (this.state.password.length < 6) {
      this._showToast('Password should atleast 6 characters');
    } else if (this.state.password === null || this.state.password === '') {
      this._showToast('Please Enter Password');
    } else {
      //this._showLoader();
      axios.post('/userResetPassword', {
        mailcode: this.state.code,
        password: this.state.password,
      })
      .then((response) => {
        if (response.data.code === 200) {
          this._hideLoader();
          Toast.show('Password Reset Successfully', {
              duration: Toast.durations.SHORT,
              position: Toast.positions.BOTTOM,
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
              onHidden: () => {
                    Actions.Signin();
                  },
            });
        }

        if (response.data.code === 403) {
          this._hideLoader();
          Toast.show(response.data.message, {
              duration: Toast.durations.SHORT,
              position: Toast.positions.BOTTOM,
              shadow: true,
              animation: true,
              hideOnPress: true,
              delay: 0,
            });
        }
      })
      .catch(function (error) {
        console.log(error);
      });
    }
  }

  _renderButton() {
      if (this.state.animating === true) {
        return (
          <Button block disabled>
            <Text style={styles.themeButtonText}>Reset Password</Text>
          </Button>
        );
      } else {
        return (
          <Button block style={styles.loginButton} onPress={this._sendEmail.bind(this)}>
            <Text style={styles.themeButtonText}>Reset Password</Text>
          </Button>
        );
      }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
            <View style={styles.loginView}>
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.forgetPwStatusBar} noShadow={true}>
                    <Left>
                        <Button transparent  onPress={this._goBack.bind(this)}>
                            <Icon name='arrow-back' style={styles.themeBackButton}/>
                        </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.themeHeaderTitle}>Reset Password</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content>
                    <View style={styles.forgetPwImageView}>
                      <Image style={styles.themeLogoImage} source={logoImg}/>
                    </View>
                    <View style={styles.loginInputView}>
                      <Form>
                        <Item floatingLabel style={styles.loginInput}>
                            <Label style={styles.themeText}>Code</Label>
                            <Input style={styles.themeText}
                              autoCapitalize='none'
                              keyboardType='numeric'
                              onChangeText={(value) =>
                                {
                                  this.setState({
                                    code: value.trim(),
                                  });
                                }
                              }/>
                        </Item>
                        <Item floatingLabel style={styles.loginInput}>
                            <Label style={styles.themeText}>Enter New Password</Label>
                            <Input secureTextEntry={true} style={styles.themeText}
                              onChangeText={(value) =>
                                {
                                  this.setState({
                                    password: value.trim(),
                                  });
                                }
                              }/>
                        </Item>
                      </Form>
                    </View>
                    <ActivityIndicator
                      animating= {this.state.animating}
                      color='#D93F49'
                      size='large'
                    />
                    {this._renderButton()}
                </Content>
              </Container>
          </View>
        </TouchableWithoutFeedback>
          );
  }
}

export default ResetPw;
