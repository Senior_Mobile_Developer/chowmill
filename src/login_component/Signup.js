import React, { Component } from 'react';
import { StyleSheet, View, Image, WebView, Alert,
TouchableWithoutFeedback, Keyboard } from 'react-native';
import { Container, Content, Form, Item,
   Input, Label, Button, Text,
   Header, Left, Icon, Body, Title, Right, } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import styles from '../style/style';
import axios from 'axios';
import Config from '../config/Config';
import Modal from 'react-native-modalbox';

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

const logoImg = require('../assets/logo1.png');
let weburlpp = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3000/privacypolicy';
let weburltos = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3000/termsofservice';
class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showToast: false,
      name: null,
      email: null,
      password: null,
      cpassword: null,
      weburlpp: weburlpp,
      weburltos: weburltos,
    };
  }

  _goBack() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
  }

  _validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  _openModal() {
    Alert.alert(
      'ChowMill',
      'Do you agree with all terms of service & privacy policy of ChowMill?',
      [
        { text: 'Yes, I agree', onPress: () => this._sendData() },
        { text: 'Terms of service', onPress: () => this.refs.termsofservicemodal.open() },
        { text: 'Privacy policy', onPress: () => this.refs.privacypolicymodal.open() },
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },
      ],
      { cancelable: true }
    );
  }

  _closeModal(f) {
    if (f === 'tos') {
      this.refs.termsofservicemodal.close();
    }

    if (f === 'pp') {
      this.refs.privacypolicymodal.close();
    }
  }

  _sendData() {
    console.log('SEND DATA...!!!');
    axios.post('/userRegister', {
      username: this.state.name,
      mail: this.state.email,
      password: this.state.password,
    })
    .then(function (response) {
      if (response.data.code === 200) {
        Toast.show('Successfully Registered', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onHidden: () => {
                  Actions.Signin();
                },
          });
      }

      if (response.data.code === 403) {
        Toast.show('User Already Exists\nPlease Login', {
            duration: Toast.durations.SHORT,
            position: Toast.positions.BOTTOM,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onHidden: () => {
                  Actions.Signin();
                },
          });
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  _onSignup() {
    if (this.state.name === null || this.state.name === '') {
      this._showToast('Please Enter Name');
    }else if (this.state.email === null || this.state.email === '') {
      this._showToast('Please Enter Email');
    }else if (this.state.password === null || this.state.password === '') {
      this._showToast('Please Enter Password');
    } else if (this.state.cpassword === null || this.state.cpassword === '') {
      this._showToast('Please Enter Confirm Password');
    } else if (this.state.password.length < 6) {
      this._showToast('Password should atleast 6 characters');
    } else {
      if (this.state.cpassword !== this.state.password) {
        this._showToast('Password Doesnot Match');
      } else if (this.state.name.indexOf(' ') <= 0) {
        this._showToast('Please Enter Full Name');
      } else if (!this._validateEmail(this.state.email)) {
        this._showToast('Please Enter Valid Email address');
      } else {
        this._openModal();
      }
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
            <View style={styles.loginView}>
              <Container style={styles.themeBackgroundWhite}>
              <Content>
              <View style={styles.loginInputView}>
                <Image style={styles.themeLogoImage} source={logoImg}/>
                <Form>
                  <Item floatingLabel style={styles.loginInput}>
                      <Label style={styles.themeText}>Full Name</Label>
                      <Input style={styles.themeText}
                        onChangeText={(value) =>
                          {
                            this.setState({
                              name: value.trim(),
                            });
                          }
                        }/>
                  </Item>
                  <Item floatingLabel style={styles.loginInput}>
                      <Label style={styles.themeText}>Email</Label>
                      <Input keyboardType='email-address'
                        autoCapitalize='none'
                        style={styles.themeText}
                        onChangeText={(value) =>
                          {
                            this.setState({
                              email: value.toLowerCase().trim(),
                            });
                          }
                        }/>
                  </Item>
                  <Item floatingLabel style={styles.loginInput}>
                      <Label style={styles.themeText}>Password</Label>
                      <Input secureTextEntry={true} style={styles.themeText}
                        onChangeText={(value) =>
                          {
                            this.setState({
                              password: value.trim(),
                            });
                          }
                        }/>
                  </Item>
                  <Item floatingLabel style={styles.loginInput}>
                      <Label style={styles.themeText}>Confirm Password</Label>
                      <Input secureTextEntry={true} style={styles.themeText}
                        onChangeText={(value) =>
                          {
                            this.setState({
                              cpassword: value.trim(),
                            });
                          }
                        }/>
                  </Item>
                </Form>
              </View>
              <Button block style={styles.loginButton}
                onPress={this._onSignup.bind(this)}>
                  <Text style={styles.themeButtonText}>Sign Up</Text>
              </Button>
              <View style={styles.signUpBottomText}>
                <Label
                  style={styles.signUpContentText}
                  onPress={this._goBack.bind(this)}
                  > Already have an account?
                <Label style={styles.signUpContentLoginText}
                  > Login here</Label>
                </Label>
              </View>
            </Content>
            </Container>
            <Modal
                  ref={'termsofservicemodal'}
                  swipeToClose={false}
                  onClosed={this.onClose}
                  onOpened={this.onOpen}
                  position={'top'}
                  >
                <Container>
                  <Header style={styles.purchaseStatusBar} noShadow={true}>
                    <Left/>
                    <Body style={styles.themeHeaderTitleBody}>
                      <Title style={styles.themeHeaderTitle}>Terms Of Service</Title>
                    </Body>
                    <Right>
                      <Button transparent onPress={() => this._closeModal('tos')}>
                        <Icon name='close' style={styles.themeBackButton}/>
                      </Button>
                    </Right>
                  </Header>
                  <WebView
                        source = {{ uri: this.state.weburltos }}
                        startInLoadingState = {true}
                        />
                </Container>
              </Modal>
              <Modal
                    ref={'privacypolicymodal'}
                    swipeToClose={false}
                    onClosed={this.onClose}
                    onOpened={this.onOpen}
                    position={'top'}
                    >
                  <Container>
                    <Header style={styles.purchaseStatusBar} noShadow={true}>
                      <Left/>
                      <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.themeHeaderTitle}>Privacy Policy</Title>
                      </Body>
                      <Right>
                        <Button transparent onPress={() => this._closeModal('pp')}>
                          <Icon name='close' style={styles.themeBackButton}/>
                        </Button>
                      </Right>
                    </Header>
                    <WebView
                          source = {{ uri: this.state.weburlpp }}
                          startInLoadingState = {true}
                          />
                  </Container>
              </Modal>
          </View>
        </TouchableWithoutFeedback>
          );
  }
}

export default Signup;
