import React, { Component } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Container, Content, Form, Item, Input, Label, Button, Text, Toast } from 'native-base';
import { Scene, Router } from 'react-native-router-flux';
import styles from '../style/style';
const logoImg = require('../assets/splash.png');

class Splash extends Component {
  render() {
    return (
          <View style={styles.splashView}>
            <Image style={styles.themeLogoImage} source={logoImg}/>
            <Label style={styles.splashLoadingText}>Loading...</Label>
          </View>
        );
  }
}

export default Splash;
