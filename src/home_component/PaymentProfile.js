import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  ListView,
  RefreshControl,
  View
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Header,
  Right,
  Title,
  Label,
  Button,
  Icon
} from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import Popup from 'react-native-popup';
import axios from 'axios';
import Config from '../config/Config';

var cardData = null;
let paymentProfileToken = null;
var TOKEN = 'token';

class PaymentProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      refreshing: false,
      noData: false,
    };
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState () {
    try {
      let value = await AsyncStorage.getItem(TOKEN);
      paymentProfileToken = value;
      axios.defaults.headers.common.token = value;
      axios.get('/api/getCardDetails').then((response) => {
        if (response.data.code === 200) {
          cardData = response.data.data;
          this.setState({ isLoading: false, data: cardData });
        }

        if (response.data.code === 403) {
          this.setState({ isLoading: false, noData: true });
          this._showToast('No data found.');
        }
      }).catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this._loadInitialState().then(() => {
      this.setState({ refreshing: false });
    });
  }

  _refreshControl() {
    return (
      <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
          tintColor="#D93F49"
          title="Loading..."
          titleColor="#D93F49"
          />
      );
  }

  _back() {
    Actions.pop();
  }

  _askForDelete(listData) {
    this.popup.confirm({
        title: 'ChowMill',
        content: 'Do you want to remove this Card?',
        ok: {
          text: 'Yes',
          style: {
            color: 'red',
          },
          callback: () => {
            this._delete(listData);
          },
        },
      });
  }

  _delete(listData) {
    this.setState({ isLoading: true });
    axios.defaults.headers.common.token = paymentProfileToken;
    var data = {
      cardId: listData.cardId,
    };
    axios.post('/api/postCardDetails', data).then((response) => {
      if (response.data.code === 200) {
        cardData.splice(listData.findex - 1, 1);
        this.setState({ isLoading: false, data: cardData });
        this._showToast('Deleted Successfully');
        this._loadInitialState();
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  _renderRows(listData) {
    return (
      <Card>
        <CardItem style={styles.purchaseProfilePageCardItem}>
          <View style={styles.purchaseCardCotainer}>
            <Label style={styles.purchaseProfileHeaderColor}>CARD TYPE:
              <Text style={styles.purchaseProfileContentColor}> {listData.type}</Text>
            </Label>
            <Label style={styles.purchaseProfileHeaderColor}>CARD NUMBER:
              <Text style={styles.purchaseProfileContentColor}> xxxx xxxx xxxx {listData.cardNumber}
              </Text>
            </Label>
            <Label style={styles.purchaseProfileHeaderColor}>EXPIRY DATE:
              <Text style={styles.purchaseProfileContentColor}> {listData.expiryDate}
              </Text>
            </Label>
          </View>
          <View>
            <Right>
                <Icon
                  onPress={() => this._askForDelete(listData)}
                  name="close-circle"
                  style={styles.purchaseProfileCloseColor}
                />
            </Right>
          </View>
        </CardItem>
      </Card>
    );
  }

  render() {
    if (this.state.isLoading === true) {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Payment Profile</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content>
                      <ActivityIndicator
                        animating={this.state.isLoading} color='#D93F49' size='large'/>
                </Content>
              </Container>
          );
    } else if (this.state.noData === true) {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Payment Profile</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content>
                  <Card>
                    <CardItem style={styles.purchaseProfilePageCardItem}>
                      <Text style={styles.purchaseProfileContentColor}>
                        No Card Found Please Purchase Food</Text>
                    </CardItem>
                  </Card>
                </Content>
              </Container>
          );
    } else {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Payment Profile</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content refreshControl={this._refreshControl()}>
                  <ListView dataSource={this.ds.cloneWithRows(this.state.data)}
                    renderRow={(listData) =>
                      this._renderRows(listData)
                    } />
                </Content>
                <Popup ref={popup => this.popup = popup }/>
              </Container>
          );
    }
  }
}

export default PaymentProfile;
