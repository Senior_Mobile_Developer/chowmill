import React, { Component } from 'react';
import {
  Image,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Picker
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Button,
  Icon,
  Header,
  Right,
  Title,
  Label,
  Item,
  Input,
  ListItem,
} from 'native-base';
import styles from '../style/style';
import axios from 'axios';
import Config from '../config/Config';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import RadioForm, { RadioButton, RadioButtonInput,
  RadioButtonLabel } from 'react-native-simple-radio-button';
import CheckBox from 'react-native-check-box';
import Modal from 'react-native-modalbox';
import { CreditCardInput, LiteCreditCardInput } from 'react-native-credit-card-input';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { encode, decode } from './EncDec';

let cardData = null;
var TOKEN = 'token';
var GTOKEN = null;
var UADD = 'address';
axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

var radioProps = [
  {
    label: 'no tip ',
    value: 0,
  }, {
    label: '10% ',
    value: 10,
  }, {
    label: '15% ',
    value: 15,
  }, {
    label: '20% ',
    value: 20,
  },
];
var payRadioProps = [
  {
    label: 'New Card ',
    value: 'newcard',
  },
  {
    label: 'Existing Card ',
    value: 'existingcard',
  },
];

var existingCardData = null;

class PurchaseScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      foodId: this.props._id,
      sellermail: this.props.sellermail,
      foodname: this.props.foodname,
      radioValue: 15,
      price: this.props.price,
      qty: '1',
      qtyPrice: this.props.price * 1,
      tip: this.props.price * 0.15,
      deliveryprice: 0,
      address: this.props.address,
      editAddress: false,
      disableClose: true,
      data: '',
      animating: false,
      saveThisCard: true,
      visibleNewCard: true,
      visibleExistingCard: true,
      selectedCard: null,
      dbExistingCards: undefined,
      selectedExistingCardIf: 0,
      payRadioPropsState: undefined,
      street: undefined,
      city: undefined,
      state: undefined,
      zipcode: undefined,
    };
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState () {
    try {
      let value = await AsyncStorage.getItem(TOKEN);
      GTOKEN = value;
      axios.defaults.headers.common.token = value;
      axios.get('/api/getCardDetails').then((response) => {
        if (response.data.code === 200) {
          existingCardData = response.data.data;
          this.setState({
            dbExistingCards: existingCardData,
            selectedExistingCardIf: 1,
            disableClose: false,
            selectedCard: response.data.data[0].cardId,
            payRadioPropsState: payRadioProps,
          });
        }

        if (response.data.code === 403) {
          console.log("getCardDetails:",response.data);
          this.setState({
            selectedExistingCardIf: 0
          });
          // if (this.state.payRadioPropsState) {
          //   this.state.payRadioPropsState.splice(1, 1);
          // }
        }
      }).catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.error('Error:', error.message);
    }
  };

  _back() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  }

  _changeAddress() {
    if (!this.state.editAddress) {
      return (<CardItem style={styles.purchaseCardItem}><Item regular><Input disabled
        placeholder= {(this.state.address === null) ? 'Enter Your Address' : this.state.address}
        style={styles.purchaseContentText}/></Item>
    </CardItem>);
    } else {
      return (
        <CardItem style={styles.purchaseAddressContent}>
         <Item regular>
            <Input
              placeholder='Street Address'
              selectTextOnFocus={true}
              autoFocus={true} onChangeText={(value) => {
              this.setState({ street: value.trim() });
            }} style={styles.purchaseContentText}/>
          </Item>
          <Item regular>
            <Input
              placeholder='City'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ city: value.trim() });
            }} style={styles.purchaseContentText}/>
            <Input
              placeholder='State'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ state: value.trim() });
            }} style={styles.purchaseContentText}/>
            <Input
              keyboardType='numeric'
              placeholder='Zip code'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ zipcode: value.trim() });
            }} style={styles.purchaseContentText}/>
          </Item>
      </CardItem>
      );
    }
  }

  _toggleAddress() {
    this.setState({
      address: (this.state.editAddress) ? this.props.address : null,
      street: undefined,
      city: undefined,
      state: undefined,
      zipcode: undefined,
      editAddress: !this.state.editAddress,
    });
  }

  _validateZipcode(zip) {
    var re = /^\d{5}$/;
    return re.test(zip);
  }

  _check() {
    if (this.state.address === null || this.state.address === '') {
      if(this.state.address === '') {
        this._showToast('Please Enter Address');
      } else if (this.state.street === undefined) {
        this._showToast('Please Enter Street Address');
      } else if (this.state.city === undefined) {
        this._showToast('Please Enter City');
      } else if (this.state.state === undefined) {
        this._showToast('Please Enter State');
      } else if (this.state.zipcode === undefined) {
        this._showToast('Please Enter Zipcode');
      } else if (!this._validateZipcode(this.state.zipcode)) {
        this._showToast('Please Enter Valid Zipcode');
      } else {
        this.setState({
          address: this.state.street+','+this.state.city+','+this.state.state+','+this.state.zipcode,
        },this._checkFood);
        // this._checkFood();
      }
    } else {
        if ((this.state.qtyPrice + this.state.deliveryprice + this.state.tip) === 0) {
          this._showToast('Please Enter Valid Qty');
        } else {
          this._checkFood();
        }
      }
  }

  _checkFood() {
    axios.defaults.headers.common.token = GTOKEN;
    let data = {
      foodId: this.state.foodId,
      qty: this.state.qty,
      deliveryAddress: this.state.address
    };
    axios.post('/api/getFoodQty', data).then((response) => {
      if (response.data.code === 200) {
        this.refs.ccmodal.open();
      }

      if (response.data.code === 403) {
        this._showToast(response.data.message);
      }

      if (response.data.code === 404) {
        this._showToast(response.data.message);
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  /************************** Pay Money Modal ****************/

  // onClose() {
  //     console.log('Modal Close');
  // }

  // onOpen() {
  //     console.log('Modal Open');
  // }

  // onClosingState(state) {
  //     console.log('Modal is Closing.....');
  // }

  _closeModal() {
    this.refs.ccmodal.close();
    cardData = null;
    this.setState({ data: '', animating: false});
  }

  _onChange = formData => {
    if (formData.valid === true) {
      this.setState({ disableClose: false });
      cardData = formData;
    } else {
      this.setState({ disableClose: true });
      cardData = '';
    }
  };

  // _onFocus = field => {
  //   console.log(field);
  // };

  _pay(cardStatus) {
    if (cardStatus === 'new') {
      // console.log('Final Result:', JSON.stringify(cardData, null, ' '));
      this.setState({
        data: JSON.stringify(cardData, null, ' '),
        animating: true,
      });
      this._placeOrder(cardStatus);
    }

    if (cardStatus === 'existing') {
      this.setState({
        animating: true,
      });
      this._placeOrder(cardStatus);
    }
  }

  _saveThisCard(data) {
    this.setState({ saveThisCard: data });
    this._placeOrder(cardStatus);
  }

  _showCheckBox() {
    if (this.state.disableClose === false) {
      return (
        <ListItem>
          <CheckBox
                style={{ flex: 1, paddingRight: 30 }}
                isChecked={this.state.saveThisCard}
                onClick={() => this._saveThisCard(!this.state.saveThisCard)}
              />
          <Text>Save this card for future payment</Text>
        </ListItem>
      );
    } else {
      return null;
    }
  }

  _payButton(cardStatus) {
    if (this.state.disableClose === false) {
      return (
          <Button block style={styles.themeButton} onPress={() => this._pay(cardStatus)} >
              <Text style={styles.themeButtonText}>Pay</Text>
          </Button>
      );
    } else {
      return (
          <Button disabled block>
              <Text style={styles.themeButtonText}>Pay</Text>
          </Button>
      );
    }
  }

  _setCard(value) {
    if (value === 'newcard') {
      this.setState({
        visibleNewCard: true,
        visibleExistingCard: false,
        disableClose: true,
        selectedExistingCardIf: 0
      });
    } else {
      this.setState({
        visibleNewCard: false,
        visibleExistingCard: true,
        disableClose: false,
        selectedExistingCardIf: 1
      });
    }
  }

  _newCard() {
    if (this.state.visibleNewCard === true && this.state.selectedExistingCardIf === 0 ) {
      return (
        <View style={styles.purchaseModalView}>
          <LiteCreditCardInput
            requiresCVC
            validColor={'black'}
            invalidColor={'red'}
            placeholderColor={'darkgray'}
            onChange={this._onChange}
            onFocus={this._onFocus}
          />
          <ActivityIndicator
            animating= {this.state.animating}
            color='#D93F49'
            size='large'
          />
          {/* {this._showCheckBox()} */}
          {this._payButton('new')}
        </View>
      );
    } else {
      return null;
    }
  }

  _showSelectedCardInfo() {
    for (let i = 0; i < Object.keys(this.state.dbExistingCards).length; i++) {
      if (this.state.selectedCard === this.state.dbExistingCards[i].cardId) {
        return (
            <View>
              <Label>CARD TYPE: <Text
                style={{ color: 'darkgray' }}>
                {this.state.dbExistingCards[i].type}</Text>
              </Label>
              <Label>CARD NUMBER: <Text
                style={{ color: 'darkgray' }}>
                xxxx xxxx xxxx {this.state.dbExistingCards[i].cardNumber}</Text>
              </Label>
              <Label>EXPIRY DATE: <Text
                style={{ color: 'darkgray' }}>
                {this.state.dbExistingCards[i].expiryDate}</Text>
              </Label>
          </View>);
      }
    }
  }

  _setSelectedCard(value) {
    this.setState({ selectedCard: value });
  }

  _existingCard() {
    if (this.state.visibleExistingCard === true && this.state.selectedExistingCardIf === 1) {
      return (
        <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    padding: 10,
                  }}>
          <Picker
            selectedValue={this.state.selectedCard}
            onValueChange={(value) => this._setSelectedCard(value)}>
            {this.state.dbExistingCards.map((listData, index) =>
             {
                return (
                <Picker.Item
                  key={index}
                  label={(index + 1) + ') ' + listData.type }
                  value={listData.cardId} />
                );
              })
            }
          </Picker>
          {this._showSelectedCardInfo()}
          <ActivityIndicator
            animating= {this.state.animating}
            color='#D93F49'
            size='large'
          />
          {this._payButton('existing')}
        </View>
      );
    } else {
      return null;
    }
  }

  /************************** Pay Money Modal ****************/

  async _placeOrder (cardStatus) {
    this.setState({disableClose:true});
    let value = await AsyncStorage.getItem(TOKEN);
    axios.defaults.headers.common.token = value;
    var data = {
      foodId: this.state.foodId,
      sellermail: this.state.sellermail,
      foodName: this.state.foodname,
      qty: this.state.qty,
      price: this.state.qtyPrice,
      deliveryFee: this.state.deliveryprice,
      tip: this.state.tip,
      totalPrice: (this.state.qtyPrice + this.state.deliveryprice + this.state.tip),
      deliveryAddress: this.state.address,
      payToken: (cardStatus === 'new') ? encode(JSON.stringify(this.state.foodId), JSON.stringify(cardData)) : null,
      cardId: (cardStatus === 'existing') ? encode(JSON.stringify(this.state.foodId), JSON.stringify(this.state.selectedCard)) : null,
    };
    axios.post('/api/placeOrder', data).then((response) => {
      if (response.data.code === 200) {
        try {
          this._closeModal();
          AsyncStorage.setItem(UADD, this.state.address);
          Actions.PurchaseConfirm(response.data.data);
        } catch (error) {
          console.error('Error:AsyncStorage:', error.message);
        }
      }

      if (response.data.code === 403) {
        this._showToast('Sorry only ' + response.data.qty + ' items available');
      }

      if (response.data.code === 400) {
        this._showToast(response.data.message);
      }

      if (response.data.code === 402) {
        this._showToast(response.data.message);
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  render() {
    return (
      <View style={styles.purchaseView}>
        <Container style={styles.themeBackgroundWhite}>
          <Header style={styles.purchaseStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
            <Left>
              <Button transparent onPress={this._back.bind(this)}>
                <Icon name='arrow-back' style={styles.themeBackButton}/>
              </Button>
            </Left>
            <Body style={styles.themeHeaderTitleBody}>
              <Title style={styles.themeHeaderTitle}>Place Order</Title>
            </Body>
            <Right/>
          </Header>
          <Content>
            <Card>
              <CardItem style={styles.purchaseCardItem}>
                <Grid>
                  <Row>
                    <Col size = {5} >
                      <Text style={styles.purchaseContentText}>
                        1. {this.state.foodname}
                      </Text>
                    </Col>
                    <Col size = {1} >
                      <Label style={styles.purchaseContentTextPaddig}>
                        QTY.
                      </Label>
                    </Col>
                    <Col size = {1} >
                      <TextInput style={styles.purchaseTextInput}
                        defaultValue={this.state.qty}
                        editable={true} keyboardType='numeric'
                        selectTextOnFocus={true}
                        onChangeText={(value) => {
                        this.setState({
                          qty: value,
                          qtyPrice: Number(value * this.state.price),
                          tip: Number(((Number(value * this.state.price) * this.state.radioValue) / 100).toFixed(2)),
                        });
                      }
                    }/>
                    </Col>
                    <Col size = {2}>
                      <Text style={styles.purchaseContentTextPaddig}>
                        ${this.state.qtyPrice}
                      </Text>
                    </Col>
                  </Row>
                </Grid>

              </CardItem>
              <CardItem header style={styles.purchaseHeadContent}>
                <Label style={styles.purchaseHeadContentText}>
                  Add Tip</Label>
              </CardItem>
              <CardItem style={styles.purchaseCardItem}>
                <RadioForm radio_props={radioProps} initial={2}
                  formHorizontal={true} labelHorizontal={true}
                  buttonColor={'#D93F49'} animation={true}
                  onPress={(value) => {
                  this.setState({
                    radioValue: value,
                    tip: Number(((this.state.qtyPrice * value) / 100).toFixed(2)),
                  });
                }
              }/>
              </CardItem>
              <CardItem style={styles.purchaseCardItem}>
                <Text style={styles.purchaseContentText}>
                  2. Tip
                </Text>
                <Label style={styles.purchaseContentText}>
                  ${(this.state.tip).toFixed(2)}
                  {/* ${((this.state.qtyPrice * this.state.radioValue) / 100).toFixed(2)} */}
                </Label>

              </CardItem>
              <CardItem footer style={styles.purchaseCardItem}>
                <Label >
                  Total
                </Label>
                <Text >
                  ${(this.state.qtyPrice + this.state.deliveryprice + this.state.tip).toFixed(2)}</Text>

              </CardItem>
            </Card>
            <Card style={styles.purchaseAddressCard}>
              <CardItem style={styles.purchaseHeadContentDelivery}>
                <Label style={styles.purchaseHeadContentText}>
                  Delivery Address
                </Label>
                <Icon active ios="ios-create-outline"
                  android="md-create"
                  defaultValue={this.state.address}
                  onPress={() => this._toggleAddress()} style={styles.themeColor}/>
              </CardItem>
              {this._changeAddress()}
            </Card>
            <Card bordered={false}>
              <CardItem style={styles.purchaseLabelItem}>
                <Label style={styles.purchaseEstimateText}>
                  Your food will be delivered before 12pm
                </Label>
              </CardItem>
            </Card>
            <Button block style={styles.themeButton} onPress={this._check.bind(this)}>
              <Text style={styles.themeButtonText}>Place Order</Text>
            </Button>
          </Content>
        </Container>
        <Modal
              ref={'ccmodal'}
              swipeToClose={false}
              onClosed={this.onClose}
              onOpened={this.onOpen}
              position={'top'}
              onClosingState={this.onClosingState}
              >
            <Container>
              <Header style={styles.purchaseStatusBar} noShadow={true}>
                <Left/>
                <Body style={styles.themeHeaderTitleBody}>
                  <Title style={styles.themeHeaderTitle}>Enter Card Details</Title>
                </Body>
                <Right>
                  <Button transparent onPress={() => this._closeModal()}>
                    <Icon name='close' style={styles.themeBackButton}/>
                  </Button>
                </Right>
              </Header>
              <Content>
                <View style={styles.purchaseModalView}>
                  <RadioForm
                    radio_props={this.state.payRadioPropsState}
                    initial={this.state.selectedExistingCardIf}
                    formHorizontal={true}
                    labelHorizontal={true}
                    buttonColor={'#D93F49'}
                    animation={true}
                    onPress={(value) => { this._setCard(value);}
                  }/>
                </View>
                {this._newCard()}
                {this._existingCard()}
              </Content>
            </Container>
          </Modal>
      </View>
    );
  }
}

export default PurchaseScreen;
