import React, {Component} from 'react';
import {ActivityIndicator, AsyncStorage, RefreshControl} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Header,
  Right,
  Title,
  Label,
  Button,
  Icon
} from 'native-base';
import styles from '../style/style';
import {Actions} from 'react-native-router-flux';
import axios from 'axios';
import Config from '../config/Config';
import {Col, Row, Grid} from 'react-native-easy-grid';


import io from 'socket.io-client';
const socket = io(Config.baseSocketURL);

var TOKEN = 'token';
var UADD = 'address';
axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

var ordersData = null;
const es = " ";

class OrdersContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      refreshing: false,
      noData: false
    };
  }

  componentWillMount() {
    this._loadInitialState().done();
    socket.on('orderRefresh',(data) => {
      this._loadInitialState().done();
    });
  }

  async _loadInitialState() {
    try {
      let value = await AsyncStorage.getItem(TOKEN);
      axios.defaults.headers.common.token = value;
      axios.get('/api/sellerOrderList').then((response) => {
        if (response.data.code === 200) {
          ordersData = response.data.data;
          this.setState({isLoading: false});
        } else if (response.data.code === 403) {
          this.setState({isLoading: false, noData: true});
        } else if (response.data.code === 404) {
          this.setState({isLoading: false, noData: true});
        } else {
          this.setState({isLoading: false, noData: true});
        }
      }).catch(function(error) {
        console.log(error);
      });
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _back() {
    Actions.pop();
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this._loadInitialState().then(() => {
      this.setState({refreshing: false});
    });
  }

  _refreshControl() {
    return (<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} tintColor="#D93F49" title="Loading..." titleColor="#D93F49"/>);
  }

  _displayHeader(datas, index) {
    if (datas.orders.length > 0) {
      return (
        <Card key={index}>
          <CardItem style={styles.ordersHeadCardItem}>
            <Text style={styles.sideMenuPageContentText}>
              Your Orders for
            </Text>
            <Label style={styles.ordersHeadText}>
              {datas.date}
            </Label>
          </CardItem>
          <Grid style={styles.orderPadTopBottom}>
            <Row style={styles.orderPadTopBottomLeft}>
              <Col>
                <Label style={styles.orderIndex}>Orders:
                  <Text>{es}{datas.totalOrders}</Text>
                </Label>
              </Col>
              <Col>
                <Label style={styles.orderIndex}>Sales:<Text>{es}${(datas.sales).toFixed(2)}</Text>
                </Label>
              </Col>
              <Col>
                <Label style={styles.orderIndex}>Locations:
                  <Text>{es}{datas.locations}</Text>
                </Label>
              </Col>
            </Row>
          </Grid>
          {datas.orders.map((order, index) => {
            return (this._displayOrders(order, index));
          })}
        </Card>
      );
    } else {
      return null;
    }
  }

  _displayOrders(order, index) {
    return (
      <Grid key={index}>
        <Col style={styles.orderItemLoop}>
          <Row>
            <Label style={styles.orderIndex}>{index + 1})
              <Label style={styles.orderIndexItemContent}>
                {es}{order.foodName}</Label>
            </Label>
          </Row>
          <Row style={styles.orderItemLoopPadding}>
            <Label style={styles.orderItemHead}>Buyer Name</Label>
          </Row>
          <Row style={styles.orderItemLoopPadding}>
            <Label style={styles.orderItemContent}>{order.buyerName}</Label>
          </Row>
          <Row style={styles.orderItemLoopPadding}>
            <Label style={styles.orderItemHead}>Buyer Address</Label>
          </Row>
          <Row style={styles.orderItemLoopPadding}>
            <Label style={styles.orderItemContent}>
              {order.deliveryAddress}</Label>
          </Row>
        </Col>
        <Col>
          <Row style={styles.orderRecieptLoop}>
            <Col>
              <Row style={styles.orderRecieptView}>
                <Label style={styles.orderRecieptContent}>QTY</Label>
                <Label style={styles.orderRecieptContent}>
                  {order.qty}
                </Label>
                <Label style={styles.orderRecieptContent}>${order.price}</Label>
              </Row>
              <Row style={styles.orderRecieptView}>
                <Label style={styles.orderRecieptContent}>Tip</Label>
                <Label style={styles.orderRecieptContent}>
                  ${order.tip.toFixed(2)}</Label>
              </Row>
              <Row style={styles.orderRecieptView}>
                <Label style={styles.orderRecieptContent}>Total</Label>
                <Label style={styles.orderRecieptContent}>
                  ${(order.sellerPrice)?order.sellerPrice:order.totalPrice}</Label>
              </Row>
            </Col>
          </Row>
        </Col>
      </Grid>
    );
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <Content>
          <ActivityIndicator animating={this.state.isLoading} color='#D93F49' size='large'/>
        </Content>
      );
    } else if (this.state.noData === true) {
      return (
        <Content refreshControl={this._refreshControl()}>
          <Card>
            <CardItem style={styles.sideMenuPageCardItem}>
              <Text style={styles.purchaseProfileContentColor}>
                No Orders Found.</Text>
            </CardItem>
          </Card>
        </Content>
      );
    } else {
      return (
        <Content refreshControl={this._refreshControl()} padder>
          {ordersData.map((datas, index) => {
            return (this._displayHeader(datas, index));
          })}
        </Content>
      );
    }

  }
}

export default OrdersContent;
