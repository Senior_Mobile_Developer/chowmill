import React, { Component } from 'react';
import { AsyncStorage, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { Container, Content, Text, List, ListItem,
  Header, Left, Body, Title, Right, Icon } from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from '../../style/style';
var KEY = 'isLIn';
var TOKEN = 'token';
var UADD = 'address';
var UTYPE = 'isSeller';
var CHECKUSER = null;
var STRIPE = 'stripeConnected';
var MAIL = 'email';

var menuOptions = [
  {
    name: 'Menu',
    icon: 'construct',
    id: 'MenuManager',
    isSeller: 'true',
  },
  {
    name: 'Payment Profile',
    icon: 'person',
    id: 'PaymentProfile',
    isSeller: 'false',
  },
  {
    name: 'Payment Profile',
    icon: 'person',
    id: 'PaymentProfileSeller',
    isSeller: 'true',
  },
  {
    name: 'Edit Address',
    icon: 'home',
    id: 'EditAddress',
    isSeller: 'false',
  },
  {
    name: 'Terms Of Service',
    icon: 'list-box',
    id: 'TermsCondition',
    isSeller: 'both',
  },
  {
    name: 'Privacy Policy',
    icon: 'key',
    id: 'PrivacyPolicy',
    isSeller: 'both',
  },
  {
    name: 'About Us',
    icon: 'information-circle',
    id: 'AboutUs',
    isSeller: 'both',
  },
  {
    name: 'Contact Us',
    icon: 'contact',
    id: 'ContactUs',
    isSeller: 'both',
  },
  {
    name: 'Logout',
    icon: 'log-out',
    id: 'Logout',
    isSeller: 'both',
  },
];

class SideNavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animating: true,
    };
    this._onClick = this._onClick.bind(this);
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async  _loadInitialState () {
    try {
      let value = await AsyncStorage.getItem(UTYPE);
      CHECKUSER = value;
      this._hideLoader();
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _showLoader() {
    this.setState({ animating: true });
  }

  _hideLoader() {
    this.setState({ animating: false });
  }

  _onClick(ids) {
    if (ids === 'MenuManager') Actions.MenuManager();this.props.closeSideBar(this);
    if (ids === 'PaymentProfile') Actions.PaymentProfile();this.props.closeSideBar(this);
    if (ids === 'PaymentProfileSeller') Actions.PaymentProfileSeller();this.props.closeSideBar(this);
    if (ids === 'EditAddress') Actions.EditAddress();this.props.closeSideBar(this);
    if (ids === 'TermsCondition') Actions.TermsCondition();this.props.closeSideBar(this);
    if (ids === 'PrivacyPolicy') Actions.PrivacyPolicy();this.props.closeSideBar(this);
    if (ids === 'AboutUs') Actions.AboutUs();this.props.closeSideBar(this);
    if (ids === 'ContactUs') Actions.ContactUs();this.props.closeSideBar(this);
    if (ids === 'Logout') {
      this.props.closeSideBar(this);
      this.props.closeSideBarWithLogout(this);
    }
  }

  _listMenu(data) {
    if (data.isSeller === CHECKUSER || data.isSeller === 'both')
    {
      return (<ListItem onPress={() => this._onClick(data.id)} icon>
        <Left>
          <Icon name={data.icon} style={styles.themeColor}/>
        </Left>
        <Body>
            <Text style={styles.themeTextWithColor}>{data.name}</Text>
        </Body>
        <Right/>
      </ListItem>);
    }

    return null;

  }

  render() {
    if (this.state.animating === false) {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Content style={styles.sideMenuView}>
                  <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
                      <Left/>
                      <Body>
                          <Title>ChowMill</Title>
                      </Body>
                      <Right/>
                  </Header>
                  <List>
                    <List dataArray={menuOptions} renderRow= {this._listMenu.bind(this)} />
                  </List>
                  </Content>
              </Container>
          );
    } else {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Content style={styles.sideMenuView}>
                  <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
                      <Left/>
                      <Body>
                          <Title>ChowMill</Title>
                      </Body>
                      <Right/>
                  </Header>
                  <List>
                    <ActivityIndicator
                      animating= {this.state.animating}
                      color='#D93F49'
                      size='large'
                    />
                  </List>
                  </Content>
              </Container>
          );
    }

  }
}

SideNavBar.propTypes = {
    closeSideBar: PropTypes.func.isRequired,
  };

export default SideNavBar;
