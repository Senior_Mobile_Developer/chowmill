import React, { Component } from 'react';
import { Linking } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Header,
  Right,
  Title,
  Label,
  Button,
  Icon
} from 'native-base';
import styles from '../style/style';
import Toast from 'react-native-root-toast';
import { Actions } from 'react-native-router-flux';

var mailTo = "mailto:support@chowmill.com?Subject='ChowMill ContactUs'";

class ContactUs extends Component {

  _back() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  }

  _contactUs() {
    Linking.canOpenURL(mailTo).then(supported => {
      if (supported) {
        Linking.openURL(mailTo);
      } else {
        this._showToast('Contact us not supported!');
      }
    });
  }

  render() {
    return (
      <Container style={styles.themeBackgroundWhite}>
        <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
          <Left>
            <Button transparent onPress={this._back.bind(this)}>
              <Icon name='arrow-back' style={styles.themeBackButton}/>
            </Button>
          </Left>
          <Body style={styles.themeHeaderTitleBody}>
            <Title style={styles.sideMenuPageTitle}>Contact Us</Title>
          </Body>
          <Right/>
        </Header>
        <Content>
          <Card>
            <CardItem style={styles.sideMenuPageCardItem} onPress={this._contactUs.bind(this)}>
              <Text style={styles.sideMenuPageContentText}>
                Press Here to contact us
              </Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default ContactUs;
