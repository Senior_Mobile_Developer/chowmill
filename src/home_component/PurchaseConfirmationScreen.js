import React, { Component } from 'react';
import { Image, TextInput, View } from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Button,
  Icon,
  Header,
  Right,
  Title,
  Label,
  Item,
  Input
} from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';

class PurchaseConfirmationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _id: this.props.foodId,
      sellermail: this.props.sellermail,
      foodName: this.props.foodName,
      qty: this.props.qty,
      qtyPrice: this.props.price,
      tip: this.props.tip,
      deliveryprice: this.props.deliveryFee,
      totalPrice: this.props.totalPrice,
      address: this.props.deliveryAddress,
      estimateDate: this.props.dateTime,
    };
  }

  _goHome() {
    Actions.Home();
  }

  render() {
    return (
      <View style={styles.purchaseConfirmView}>
        <Container style={styles.themeBackgroundWhite}>
          <Header style={styles.purchaseConfirmStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
            <Left>
              <Button transparent onPress={this._goHome.bind(this)}>
                <Icon ios="ios-home" android="md-home" style={styles.themeColor}/>
              </Button>
            </Left>
            <Body style={styles.themeHeaderTitleBody}>
              <Title style={styles.themeHeaderTitle}>Order Confirmation</Title>
            </Body>
            <Right/>
          </Header>
          <Content>
            <Card>
              <CardItem style={styles.purchaseConfirmCardItem}>
                <Text style={styles.purchaseConfirmContentText}>
                  1. {this.state.foodName}
                </Text>
                <Label style={styles.purchaseConfirmContentText}>
                  QTY.
                </Label>
                <Text style={styles.purchaseConfirmContentText}>
                  {this.state.qty}</Text>
                <Text style={styles.purchaseConfirmContentText}>
                  ${this.state.qtyPrice}
                </Text>
              </CardItem>
              <CardItem style={styles.purchaseConfirmCardItem}>
                <Text style={styles.purchaseConfirmContentText}>
                  2. Tip
                </Text>
                <Label style={styles.purchaseConfirmContentText}>
                  ${this.state.tip}
                </Label>

              </CardItem>
              <CardItem footer style={styles.purchaseConfirmCardItem}>
                <Label>
                  Total
                </Label>
                <Text>
                  ${this.state.totalPrice}
                </Text>

              </CardItem>
            </Card>
            <Card>
              <CardItem header style={styles.purchaseConfirmHeadContent}>
                <Label style={styles.purchaseConfirmHeadContentText}>
                  Delivery Address</Label>
              </CardItem>
              <CardItem style={styles.purchaseConfirmCardItem}>
                <Item regular style={styles.purchaseConfirmHeadContent}>
                  <Input disabled placeholder={this.state.address}
                    style={styles.purchaseConfirmContentText}/>
                </Item>
              </CardItem>
            </Card>
            <Card bordered={false}>
              <CardItem style={styles.purchaseConfirmLabelItem}>
                <Label style={styles.purchaseConfirmEstimateText}>
                  Your food will be delivered before 12pm
                </Label>
              </CardItem>
              <CardItem style={styles.purchaseConfirmLabelItem}>
                <Label style={styles.purchaseConfirmEstimateText}>
                  {this.state.estimateDate}
                </Label>
              </CardItem>
            </Card>
          </Content>
        </Container>
      </View>
    );
  }
}

export default PurchaseConfirmationScreen;
