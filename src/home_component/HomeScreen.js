import React, { Component } from 'react';
import {
  Image,
  Alert,
  AsyncStorage,
  ListView,
  RefreshControl,
  ActivityIndicator,
  View,
  AppState
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Thumbnail,
  Text,
  Button,
  Icon,
  Header,
  Right,
  Title,
  Drawer,
  List
} from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import OrdersContent from './OrdersContent';
import SideNavBar from './sidemenu_component';
import axios from 'axios';
import Toast from 'react-native-root-toast';
import Popup from 'react-native-popup';
import Config from '../config/Config';
import Splash from '../splashscreen_component';

import io from 'socket.io-client';
const socket = io(Config.baseSocketURL);

var KEY = 'isLIn';
var TOKEN = 'token';
var UTYPE = 'isSeller';
var UADD = 'address';
var STRIPE = 'stripeConnected';
var MAIL = 'email';
var mealList = null;
var userAddress = null;
var homeToken = null;
let GEODATA = null;

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

class HomeScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      isSeller: false,
      refreshing: false,
      stripeConnected: undefined,
      noData: false,
      noDataFood:false,
      cutOffTime: false,
      appState: AppState.currentState,
      newtime: undefined
    };
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  }

  componentWillMount() {
    this._loadInitialState().done();
    socket.on('cutoff',(data) => {
      this._callForCutOff();
    });
    socket.on('refresh',(data) => {
      this._loadInitialState().done();
    });
  }

  componentWillReceiveProps() {
    this._loadInitialState().done();
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
        this._callForCutOff();
    }
    this.setState({appState: nextAppState});
  }

  _startInterval() {
    var F = true;
    setInterval(() => {
      if(this._compareTime(this.state.newtime) === true){
        this.setState({cutOffTime:true});
        if(F === true) {
            this._showToast("Market closed today at "+this.state.newtime+". Please return back tomorrow");
            F = false;
        }

      } else {
        this.setState({cutOffTime:false});
        F = true;
      }
    }, 3000);
  }

  _showToast(msg) {
    Toast.show(msg, {
        duration: Toast.durations.LONG,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
  }

  _compareTime(newtime) {
    var chrs = new Date().getHours();
    var cmins = new Date().getMinutes();
    if(((newtime.split(':')[0]) <= chrs)){
      if(((newtime.split(':')[0]) < chrs)) {
        return true;
      } else {
        if(((newtime.split(':')[1]) <= cmins)){
          if(chrs+':'+cmins >= '23:59'){
            return false;
          } else{
            return true;
          }
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }


  _callForCutOff() {
    axios.get('/api/cutoffTime').then((res) => {
      if (res.data.code === 200) {
        if(this._compareTime(res.data.cutTime) === true){
          this._showToast("Market closed today at "+res.data.cutTime+". Please return back tomorrow");
          this.setState({cutOffTime:true,newtime:res.data.cutTime});
        }
        else{
          this.setState({cutOffTime:false,newtime:res.data.cutTime});
        }
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  async _loadInitialState() {
    try {
      let value = await AsyncStorage.getItem(TOKEN);
      homeToken = value;
      let value1 = await AsyncStorage.getItem(UTYPE);
      let value2 = await AsyncStorage.getItem(STRIPE);
      userAddress = await AsyncStorage.getItem(UADD);
      this.setState({ isSeller: value1, stripeConnected: value2 });
      if (value1 === 'false') {
        axios.defaults.headers.common.token = value;
        axios.get('/api/getChowFood').then((response) => {
          if (response.data.code === 200) {
            mealList = response.data.data;
            this.setState({ isLoading: false, data: mealList });
            if(mealList[0].itemArea === false){
              this._showToast("Sorry, we do not have service in your location. We will notify you as soon as we expand to your location");
              setTimeout(()=> {
                this._callForCutOff();
                this._startInterval();
              },4000);
            } else {
              this._callForCutOff();
              this._startInterval();
            }

          }
          if (response.data.code === 404) {
            this.setState({ isLoading: false, noData: true });
          }
          if (userAddress === null) {
            this.setState({ isLoading: false });
            Actions.EditAddress();
          }
        }).catch(function (error) {
          console.log(error);
        });
      } else {
        this.setState({ isLoading: false });
        if (this.state.stripeConnected === 'false') {
          Actions.PaymentProfileSeller();
        }
      }
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _confirmLogout() {
    this.popup.confirm({
        title: 'ChowMill',
        content: 'Are you sure you want to logout?',
        ok: {
          text: 'Yes',
          style: {
            color: 'red',
          },
          callback: () => {
            this._logOut();
          },
        },
      });
  }

  _logOut() {
    try {
      AsyncStorage.removeItem(KEY);
      AsyncStorage.removeItem(TOKEN);
      AsyncStorage.removeItem(UTYPE);
      AsyncStorage.removeItem(UADD);
      AsyncStorage.removeItem(STRIPE);
      AsyncStorage.removeItem(MAIL);
      AsyncStorage.setItem(KEY, 'false');
      Actions.Signin();
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  }

  openDrawer() {
    this._drawer._root.open();
  };

  closeDrawer() {
    this._drawer._root.close();
  };

  closeDrawerWithLogout() {
    this._drawer._root.close();
    this._confirmLogout();
  };

  _purchase(listData) {
    listData.address = userAddress;
    Actions.Purchase(listData);
  }

  _showButton(listData) {
    if (listData.qty !== 0) {
      if(listData.itemArea === false){
        return (
          <Button block small disabled>
            <Text style={styles.themeButtonText}>Buy</Text>
          </Button>
        );
      } else if(this.state.cutOffTime === true){
        return (
          <Button block small style={styles.themeDisableButton} onPress={() => this._showToast("Market closed today at "+this.state.newtime+". Please return back tomorrow")}>
            <Text style={styles.themeButtonText}>Buy</Text>
          </Button>
        );
      } else {
        return (
          <Button block style={styles.themeButton} small onPress={() => this._purchase(listData)}>
            <Text style={styles.themeButtonText}>Buy</Text>
          </Button>
        );
      }
    } else {
      return (
        <Button block small disabled>
          <Text style={styles.themeButtonText}>Soldout</Text>
        </Button>
      );
    }
  }

  _saveLikeDIslike(fid, like) {
    axios.defaults.headers.common.token = homeToken;
    var data = {
      _id: fid,
      like: like,
    };
    axios.post('/api/foodLike', data).then((response) => {
      if (response.data.code === 200) {
        console.log('DONE....!');
      }

      if (response.data.code === 403) {
        this._showToast('your like/dislike not submitted');
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  _likedislike(listData, act) {
    if (act === 'like') {
      if (listData.alreadyDisliked === true) {
        mealList[listData.findex - 1].dislike = listData.dislike - 1;
        mealList[listData.findex - 1].alreadyDisliked = false;
        mealList[listData.findex - 1].like = listData.like + 1;
        mealList[listData.findex - 1].alreadyLiked = true;
        this.setState({ data: mealList });
        this._saveLikeDIslike(listData._id, true);
      } else {
        mealList[listData.findex - 1].like = listData.like + 1;
        mealList[listData.findex - 1].alreadyLiked = true;
        this.setState({ data: mealList });
        this._saveLikeDIslike(listData._id, true);
      }
    }

    if (act === 'dislike') {
      if (listData.alreadyLiked === true) {
        mealList[listData.findex - 1].like = listData.like - 1;
        mealList[listData.findex - 1].alreadyLiked = false;
        mealList[listData.findex - 1].dislike = listData.dislike + 1;
        mealList[listData.findex - 1].alreadyDisliked = true;
        this.setState({ data: mealList });
        this._saveLikeDIslike(listData._id, false);
      } else {
        mealList[listData.findex - 1].dislike = listData.dislike + 1;
        mealList[listData.findex - 1].alreadyDisliked = true;
        this.setState({ data: mealList });
        this._saveLikeDIslike(listData._id, false);
      }
    }

  }

  _showLikeButton(listData) {
    if (listData.isPurchased === true) {
      if (listData.alreadyLiked === true) {
        return (<Button transparent>
          <Icon active name="thumbs-up" style={styles.themeLikedColor}/>
          <Text style={styles.themeTextWithLikedColor}>
            {listData.like}
          </Text>
        </Button>);
      } else {
        return (<Button transparent onPress={() => this._likedislike(listData, 'like')}>
        <Icon active name="thumbs-up" style={styles.themeColor}/>
        <Text style={styles.themeTextWithColor}>
          {listData.like}
        </Text>
      </Button>);
      }
    } else {
      return (<Button transparent>
        <Icon active name="thumbs-up" style={styles.themeDisableColor}/>
        <Text style={styles.themeTextWithDisableColor}>
          {listData.like}
        </Text>
      </Button>);
    }

  }

  _showDisLikeButton(listData) {
    if (listData.isPurchased === true) {
      if (listData.alreadyDisliked === true) {
        return (<Button transparent>
          <Icon active name="thumbs-down" style={styles.themeLikedColor}/>
          <Text style={styles.themeTextWithLikedColor}>
            {listData.dislike}
          </Text>
        </Button>);
      } else {
        return (<Button transparent onPress={() => this._likedislike(listData, 'dislike')}>
          <Icon active name="thumbs-down" style={styles.themeColor}/>
          <Text style={styles.themeTextWithColor}>
            {listData.dislike}
          </Text>
        </Button>);
      }
    } else {
      return (<Button transparent>
        <Icon active name="thumbs-down" style={styles.themeDisableColor}/>
        <Text style={styles.themeTextWithDisableColor}>
          {listData.dislike}
        </Text>
      </Button>);
    }
  }

  _showDetailClick(listData,act) {
    if(act === 'showless') {
      mealList[listData.findex - 1].showMore = false;
      this.setState({ data: mealList });
    }
    if (act === 'showmore') {
      mealList[listData.findex - 1].showMore = true;
      this.setState({ data: mealList });
    }

  }
  _showDetails(listData){
    if(listData.showMore === true){
      return(<View>
        <CardItem style={{justifyContent:'space-around',alignItems:'center'}}>
            <Button transparent onPress={()=>this._showDetailClick(listData,'showless')}>
                <Text style={styles.themeTextWithLikedColor}>Show less</Text><Icon name='arrow-dropup' style={styles.themeTextWithLikedColor}/>
            </Button>
        </CardItem>
        <CardItem style={styles.homePurchaseCard}>
          <Grid>
            <Row>
              <Text style={styles.homeMainText}>Allergens: <Text note>{listData.allergens}</Text></Text>
            </Row>
            <Row>
              <Text style={styles.homeMainText}>Desription: <Text note>{listData.foodDescription}</Text></Text>
            </Row>
          </Grid>
        </CardItem>
      </View>);
    } else {
      if(listData.allergens) {
        return(<CardItem style={{justifyContent:'space-around',alignItems:'center'}}>
            <Button transparent onPress={()=>this._showDetailClick(listData,'showmore')}>
              <Text style={styles.themeTextWithLikedColor}>Show more</Text><Icon name='arrow-dropdown' style={styles.themeTextWithLikedColor}/>
            </Button>
        </CardItem>);}
    }

  }

  _showCard(listData) {
    if (listData.itemAvailable) {
      return (<Card>
        <CardItem cardBody style={styles.homeImageCard}>
          <Image style={styles.homeImage} source={{
            uri: listData.image,
          }}/>
        </CardItem>
        <CardItem style={styles.homeDetailCard}>
          <Grid>
            <Row>
              <Col size={1}>
                {this._showLikeButton(listData)}
              </Col>
              <Col size={3} style={styles.homeMainTextCol}>
                <Text style={styles.homeMainText}>
                  {listData.foodname}
                </Text>
              </Col>
              <Col size={1}>
                {this._showDisLikeButton(listData)}
              </Col>
            </Row>
          </Grid>
        </CardItem>
        <CardItem style={styles.homePurchaseCard}>
          {(listData.totalFoodSales)? <Left><Text style={styles.homeMainText}>{listData.totalFoodSales} Sales</Text></Left>: <Left/>}
          <Body style={styles.homeBuyButton}>
            {this._showButton(listData)}
          </Body>
          <Right>
            <Text style={styles.homeMainText}>
              ${listData.price}
            </Text>
          </Right>
        </CardItem>
        {this._showDetails(listData)}
      </Card>);
    } else {
      return null;
    }
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this._loadInitialState().then(() => {
      this.setState({ refreshing: false });
    });
  }

  _refreshControl() {
    return (
      <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
          tintColor="#D93F49"
          title="Loading..."
          titleColor="#D93F49"
          />
      );
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <Drawer ref={(ref) => {
          this._drawer = ref;
        }
      } content={< SideNavBar navigator = {
          this._navigator
        }
        closeSideBar = {
          () => this.closeDrawer()
        }
        closeSideBarWithLogout = {
          () => this.closeDrawerWithLogout()
        } />} onClose={() => this.closeDrawer()}>
        <Container style={styles.homeView}>
          <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
            <Left>
              <Button transparent onPress={this.openDrawer.bind(this)}>
                <Icon name='menu' style={styles.themeContrastColor}/>
              </Button>
            </Left>
            <Body >
              <Title>ChowMill</Title>
            </Body>
            <Right>
              <Button transparent onPress={this._confirmLogout.bind(this)}>
                <Icon name='log-out' style={styles.themeContrastColor}/>
              </Button>
            </Right>
          </Header>
          <Content>
            <ActivityIndicator animating={this.state.isLoading} color='#D93F49' size='large'/>
          </Content>
        </Container>
      </Drawer>
      );
    } else if (this.state.isSeller === 'true') {
      return (
        <Drawer ref={(ref) => {
          this._drawer = ref;
        }
      } content={< SideNavBar navigator = {
          this._navigator
        }
        closeSideBar = {
          () => this.closeDrawer()
        }
        closeSideBarWithLogout = {
          () => this.closeDrawerWithLogout()
        } />} onClose={() => this.closeDrawer()}>
          <Container style={styles.homeView}>
            <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
              <Left>
                <Button transparent onPress={this.openDrawer.bind(this)}>
                  <Icon name='menu' style={styles.themeContrastColor}/>
                </Button>
              </Left>
              <Body >
                <Title>ChowMill</Title>
              </Body>
              <Right>
                <Button transparent onPress={this._confirmLogout.bind(this)}>
                  <Icon name='log-out' style={styles.themeContrastColor}/>
                </Button>
              </Right>
            </Header>
            <OrdersContent/>
            <Popup ref={popup => this.popup = popup } isOverlayClickClose={false}/>
          </Container>
        </Drawer>
      );
    } else if (this.state.noData === true) {
      return (
        <Drawer ref={(ref) => {
          this._drawer = ref;
        }
      } content={< SideNavBar navigator = {
          this._navigator
        }
        closeSideBar = {
          () => this.closeDrawer()
        } />} onClose={() => this.closeDrawer()}>
          <Container style={styles.homeView}>
            <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
              <Left>
                <Button transparent onPress={this.openDrawer.bind(this)}>
                  <Icon name='menu' style={styles.themeContrastColor}/>
                </Button>
              </Left>
              <Body >
                <Title>ChowMill</Title>
              </Body>
              <Right>
                <Button transparent onPress={this._confirmLogout.bind(this)}>
                  <Icon name='log-out' style={styles.themeContrastColor}/>
                </Button>
              </Right>
            </Header>
            <Content refreshControl={this._refreshControl()} >
              <CardItem style={styles.sideMenuPageCardItem}>
                <Text style={styles.purchaseProfileContentColor}>
                  No Foods Found.</Text>
              </CardItem>
            </Content>
            <Popup ref={popup => this.popup = popup } isOverlayClickClose={false}/>
          </Container>
        </Drawer>
      );
    } else {
      return (
        <Drawer ref={(ref) => {
          this._drawer = ref;
        }
      } content={< SideNavBar navigator = {
          this._navigator
        }
        closeSideBar = {
          () => this.closeDrawer()
        }
        closeSideBarWithLogout = {
          () => this.closeDrawerWithLogout()
        } />} onClose={() => this.closeDrawer()}>
          <Container style={styles.homeView}>
            <Header style={styles.themeStatusBar} androidStatusBarColor='#D93F49'>
              <Left>
                <Button transparent onPress={this.openDrawer.bind(this)}>
                  <Icon name='menu' style={styles.themeContrastColor}/>
                </Button>
              </Left>
              <Body>
                <Title>ChowMill</Title>
              </Body>
              <Right>
                <Button transparent onPress={this._confirmLogout.bind(this)}>
                  <Icon name='log-out' style={styles.themeContrastColor}/>
                </Button>
              </Right>
            </Header>
            <Content refreshControl={this._refreshControl()} >
              <ListView dataSource={this.ds.cloneWithRows(this.state.data)}
                renderRow={(listData) =>
                  this._showCard(listData)
                } />
            </Content>
            <Popup ref={popup => this.popup = popup } isOverlayClickClose={false}/>
          </Container>
        </Drawer>
      );
    }

  }
}

export default HomeScreen;
