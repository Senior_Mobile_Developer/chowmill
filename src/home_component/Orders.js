import React, { Component } from 'react';
import { Container, Content, Card, CardItem, Left,
  Body, Text, Header, Right, Title,
   Label, Button, Icon } from 'native-base';
import styles from '../style/style';
import OrdersContent from './OrdersContent';
import { Actions } from 'react-native-router-flux';

class Orders extends Component {

  _back() {
    Actions.pop();
  }

  render() {
    return (
            <Container style={styles.themeBackgroundWhite}>
              <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                  <Left>
                    <Button transparent onPress={this._back.bind(this)}>
                        <Icon name='arrow-back' style={styles.themeBackButton}/>
                    </Button>
                  </Left>
                  <Body style={styles.themeHeaderTitleBody}>
                      <Title style={styles.sideMenuPageTitle}>Orders</Title>
                  </Body>
                <Right/>
              </Header>
              <OrdersContent/>
            </Container>
        );
  }
}

export default Orders;
