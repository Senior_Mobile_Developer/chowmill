import React, { Component } from 'react';
import { View, AsyncStorage} from 'react-native';
import { Container, Content, Card, CardItem, Left,
  Body, Text, Header, Right, Title,
   Label,Item, Input,Button, Icon } from 'native-base';
import styles from '../style/style';
import Config from '../config/Config';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';

// let add = '2933 Glen Crow Court, San Jose, CA 95148';
var UADD = 'address';
var TOKEN = 'token';
var GTOKEN = null;
let value = null;

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

class EditAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      address: null,
      editAddress: false,
      street: undefined,
      city: undefined,
      state: undefined,
      zipcode: undefined,
    };
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  _showToast(msg) {
    Toast.show(msg, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
      });
  }

  async _loadInitialState() {
    try {
      value = await AsyncStorage.getItem(UADD);
      GTOKEN  = await AsyncStorage.getItem(TOKEN);
      this.setState({address: value});
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _back() {
    Actions.pop();
  }

  _skipThis() {
    Actions.pop();
  }

  _toggleAddress() {
    this.setState({
      address: (this.state.editAddress) ? value : null,
      street: undefined,
      city: undefined,
      state: undefined,
      zipcode: undefined,
      editAddress: !this.state.editAddress,
    });
  }

  _validateZipcode(zip) {
    var re = /^\d{5}$/;
    return re.test(zip);
  }

  _check() {
    if (this.state.address === null || this.state.address === '') {
      if(this.state.address === '') {
        this._showToast('Please Enter Address');
      } else if (this.state.street === undefined) {
        this._showToast('Please Enter Street Address');
      } else if (this.state.city === undefined) {
        this._showToast('Please Enter City');
      } else if (this.state.state === undefined) {
        this._showToast('Please Enter State');
      } else if (this.state.zipcode === undefined) {
        this._showToast('Please Enter Zipcode');
      } else if (!this._validateZipcode(this.state.zipcode)) {
        this._showToast('Please Enter Valid Zipcode');
      } else {
        this.setState({
          address: this.state.street+','+this.state.city+','+this.state.state+','+this.state.zipcode,
        },this._saveAddress);
      }
    } else {
      this._saveAddress();
    }
  }

  _saveAddress() {
    axios.defaults.headers.common.token = GTOKEN;
    let data = {
      address: this.state.address
    };
    axios.post('/api/saveAddress', data).then((response) => {
      if (response.data.code === 200) {
        this._showToast("Saved Successfully...!");
        AsyncStorage.setItem(UADD, this.state.address);
        Actions.pop({ refresh: { home: true } });
      }
      if (response.data.code === 403) {
        this._showToast(response.data.message);
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  _changeAddress() {
    if (!this.state.editAddress) {
      return (<CardItem style={styles.purchaseCardItem}><Item regular><Input disabled
        placeholder= {(this.state.address === null) ? 'Enter Your Address' : this.state.address}
        style={styles.purchaseContentText}/></Item>
    </CardItem>);
    } else {
      return (
        <CardItem style={styles.purchaseAddressContent}>
         <Item regular>
            <Input
              placeholder='Street Address'
              selectTextOnFocus={true}
              autoFocus={true} onChangeText={(value) => {
              this.setState({ street: value.trim() });
            }} style={styles.purchaseContentText}/>
          </Item>
          <Item regular>
            <Input
              placeholder='City'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ city: value.trim() });
            }} style={styles.purchaseContentText}/>
            <Input
              placeholder='State'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ state: value.trim() });
            }} style={styles.purchaseContentText}/>
            <Input
              keyboardType='numeric'
              placeholder='Zip code'
              selectTextOnFocus={true}
              onChangeText={(value) => {
              this.setState({ zipcode: value.trim() });
            }} style={styles.purchaseContentText}/>
          </Item>
      </CardItem>
      );
    }
  }

  render() {
    return (
      <Container style={styles.themeBackgroundWhite}>
        <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
            <Left>
              <Button transparent onPress={this._back.bind(this)}>
                  <Icon name='arrow-back' style={styles.themeBackButton}/>
              </Button>
            </Left>
            <Body style={styles.themeHeaderTitleBody}>
                <Title style={styles.sideMenuPageTitle}>Edit Address</Title>
            </Body>
          <Right/>
        </Header>
        <Content padder>
          <Card style={styles.purchaseAddressCard}>
            <CardItem style={styles.purchaseHeadContentDelivery}>
              <Label style={styles.purchaseHeadContentText}>
                Delivery Address
              </Label>
              <Icon active ios="ios-create-outline"
                android="md-create"
                defaultValue={this.state.address}
                onPress={() => this._toggleAddress()} style={styles.themeColor}/>
            </CardItem>
            {this._changeAddress()}
          </Card>
          <Button block style={styles.themeButton} onPress={this._check.bind(this)}>
            <Text style={styles.themeButtonText}>Save Address</Text>
          </Button>
          <Button block transparent onPress={() => this._skipThis()}>
            <Text style={styles.purchaseConfirmEstimateText}>Skip this</Text>
          </Button>
        </Content>
      </Container>
      );
  }
}

export default EditAddress;
