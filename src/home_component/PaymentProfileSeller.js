import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  View,
  WebView
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Header,
  Right,
  Title,
  Label,
  Button,
  Icon
} from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modalbox';
import Toast from 'react-native-root-toast';
import axios from 'axios';
import Config from '../config/Config';

let paymentProfileToken = null;
var TOKEN = 'token';
var STRIPE = 'stripeConnected';
var MAIL = 'email';
let weburl1 = 'http://localhost:8888/simpleStripe.html';
// let a = 'response_type=code';
// let b = 'client_id=ca_AZhU6DsUWGuihgh2w5WnGZrufCiqKNpA';
// let cb = 'client_id=ca_AE1uBr2nGeAv7UUJ2xN9tz15aMFAXpfN';
// let c = 'scope=read_write';
// let d = 'state=';
// var stripeConnectUrl = 'https://connect.stripe.com/oauth/authorize?'
// + a
// + '&'
// + cb
// + '&'
// + c
// + '&'
// + d;
let stripeConnectUrl = 'https://connect.stripe.com/oauth/authorize?response_type=code&';
let localCI = 'client_id=ca_AZhU6DsUWGuihgh2w5WnGZrufCiqKNpA&scope=read_write&state=';

let serverCID = 'client_id=ca_AE1uBr2nGeAv7UUJ2xN9tz15aMFAXpfN&scope=read_write&state=';
let serverCIP = 'client_id=ca_AE1unEG5ZohlAsRZxfUCdnMaTvwFsDQd&scope=read_write&state=';
let prefill = '&stripe_user[url]=https://chowmill.com'
+ '&stripe_user[business_type]=sole_prop'
// + '&stripe_user[phone_number]=4083209594'
+ '&stripe_user[product_description]=FoodDelivery'
+ '&stripe_user[business_name]=CHOWMILL INC.'
+ '&stripe_user[email]=';

axios.defaults.baseURL = Config.baseURL;
axios.defaults.headers.post['Content-Type'] = 'application/json';

/***** PATCH for Window.post Message *****/
const patchPostMessageFunction = function() {
var originalPostMessage = window.postMessage;

var patchedPostMessage = function(message, targetOrigin, transfer) {
  originalPostMessage(message, targetOrigin, transfer);
};

patchedPostMessage.toString = function() {
  return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
};

window.postMessage = patchedPostMessage;
};

const patchPostMessageJsCode = '(' + String(patchPostMessageFunction) + ')();';
/***** PATCH for Window.post Message *****/


class PaymentProfileSeller extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      stripeConnected: undefined,
      stripeData: undefined,
      webURL: undefined,
    };
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState () {
    try {
      let value = await AsyncStorage.getItem(STRIPE);
      let value1 = await AsyncStorage.getItem(TOKEN);
      let value2 = await AsyncStorage.getItem(MAIL);
      this.setState({ webURL: stripeConnectUrl + serverCIP + value2 + prefill + value2});
      // this.setState({ webURL: 'http://localhost:8888/simpleStripe.html'});
      this.setState({ stripeConnected: value });
      axios.defaults.headers.common.token = value1;
      axios.get('/api/sellerStripeData').then((response) => {
        if (response.data.code === 200) {
          this.setState({
            stripeData: response.data.data,
            isLoading: false,
          });
        }

        if (response.data.code === 403) {
          this.setState({ isLoading: false, });
          this._showToast(response.data.message);
        }

        if (response.data.code === 404) {
          this.setState({ isLoading: false, });
          this._showToast(response.data.message);
        }
      }).catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _back() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  }

  _openModal() {
    console.log(this.state.webURL);
    this.refs.ccmodal.open();
  }

  _closeModal() {
    this.refs.ccmodal.close();
  }

  _gotMsg (event) {
    console.log(event.nativeEvent.data);
    if (event.nativeEvent.data === 'Success') {
      this._closeModal();
      this.setState({ stripeConnected: 'true' });
      try {
        AsyncStorage.setItem(STRIPE, JSON.stringify(true));
      } catch (error) {
        console.error('Error:AsyncStorage:', error.message);
      }
    }
  }

  _stripeConnection () {
    if (this.state.stripeConnected === 'true') {
      return (
        <Card>
          {/* <CardItem>
            <Label>Stripe Details</Label>
            <Text>{this.state.stripeData}</Text>
          </CardItem> */}
          <CardItem>
            <Text style = {styles.themeText}>Successfully Connected With Stripe</Text>
          </CardItem>
          <Button block disabled success>
            <Text>Connected with stripe</Text>
          </Button>
        </Card>);
    } else {
      return (
        <Card>
          <CardItem>
            <Text style = {styles.themeText}>Please Connect With Stripe</Text>
          </CardItem>
          <Button block primary onPress={() => this._openModal()}>
          <Text>Connect with stripe</Text>
        </Button>
        </Card>);
    }
  }

  render() {
    if (this.state.isLoading === true) {
      return (
              <Container>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Payment Profile</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content>
                      <ActivityIndicator
                        animating={this.state.isLoading} color='#D93F49' size='large'/>
                </Content>
              </Container>
          );
    } else {
      return (
              <Container>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Payment Profile</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content style={{ margin: 10 }}>
                  {this._stripeConnection()}
                </Content>
                <Modal
                      ref={'ccmodal'}
                      swipeToClose={false}
                      onClosed={this.onClose}
                      onOpened={this.onOpen}
                      position={'top'}
                      onClosingState={this.onClosingState}
                      >
                    <Container>
                      <Header style={styles.purchaseStatusBar} noShadow={true}>
                        <Left/>
                        <Body style={styles.themeHeaderTitleBody}>
                          <Title style={styles.themeHeaderTitle}>Stripe Connect</Title>
                        </Body>
                        <Right>
                          <Button transparent onPress={() => this._closeModal()}>
                            <Icon name='close' style={styles.themeBackButton}/>
                          </Button>
                        </Right>
                      </Header>
                      <WebView
                            source = {{ uri: this.state.webURL }}
                            onMessage = {(event)=>this._gotMsg(event)}
                            startInLoadingState = {true}
                            injectedJavaScript={patchPostMessageJsCode}
                            renderError = {()=> console.log('Errorrrrr')}
                            />
                    </Container>
                  </Modal>
              </Container>
          );
    }
  }
}

export default PaymentProfileSeller;
