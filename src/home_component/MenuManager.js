import React, { Component } from 'react';
import {
  Image,
  TextInput,
  View,
  Switch,
  ListView,
  ActivityIndicator,
  AsyncStorage,
  RefreshControl
} from 'react-native';
import {
  Container,
  Content,
  Card,
  CardItem,
  Left,
  Body,
  Text,
  Button,
  Icon,
  Header,
  Right,
  Title,
  Label,
  Item,
  Input,
  List
} from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-root-toast';
import Popup from 'react-native-popup';
import axios from 'axios';
import Config from '../config/Config';
import { Col, Row, Grid } from 'react-native-easy-grid';

var TOKEN = 'token';
var LocalToken = '';
var menuData = '';

class MenuManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      refreshing: false,
      noData: false,
    };
    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
  }

  componentWillMount() {
    this._loadInitialState().done();
  }

  async _loadInitialState () {
    try {
      let value = await AsyncStorage.getItem(TOKEN);
      LocalToken = value;
      axios.defaults.headers.common.token = value;
      axios.get('/api/getSellerFood').then((response) => {
        menuData = response.data.data;
        if (response.data.code === 200) {
          this.setState({ data: menuData, disableAll: menuData[0].disableAll });
          this.setState({ isLoading: false });
        }

        if (response.data.code === 403) {
          this.setState({ isLoading: false, noData: true });
          this._showToast(response.data.message);
        }
      }).catch(function (error) {
        console.log(error);
      });
    } catch (error) {
      console.error('Error:AsyncStorage:', error.message);
    }
  };

  _back() {
    Actions.pop();
  }

  _showToast(msg) {
    Toast.show(msg, {
      duration: Toast.durations.SHORT,
      position: Toast.positions.BOTTOM,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
    });
  }

  _changeData(foodId, qty, itemAvailable) {
    axios.defaults.headers.common.token = LocalToken;
    var data = {
      foodId: foodId,
      qty: qty,
      itemAvailable: itemAvailable,
    };
    axios.post('/api/menuManagerList', data).then((response) => {
      if (response.data.code === 200) {
        this._showToast('Successfully changed...');
      }
    }).catch(function (error) {
      console.log(error);
    });
  }

  /*** Toggle Quanity ****/
  _onEnd(event, findex, foodId, qty, itemAvailable) {
    var newVal = event.nativeEvent.text;
    this.popup.confirm({
        title: 'ChowMill',
        content: 'Do you want to proceed?',
        ok: {
          text: 'Yes',
          style: {
            color: 'red',
          },
          callback: () => {
            this._toggleQtyChange(findex, newVal, foodId, qty, itemAvailable);
          },
        },
        cancel: {
          callback: () => {
            this._toggleQtyCancel(findex, qty);
          },
        },
      });
  }

  _toggleQtyChange(findex, newVal, foodId, qty, itemAvailable) {
    menuData[findex - 1].qty = newVal;
    this.setState({ data: menuData });
    this._changeData(foodId, newVal, itemAvailable);
  }

  _toggleQtyCancel(findex, oldvalue) {
    oldvalue = oldvalue.toString();
    this.setState({ x: true });
    menuData[findex - 1].qty = oldvalue;
    this.setState({ data: menuData });
  }

  _setx() {
    this.setState({ x: false });
  }

  _inputQty(itemAvailable, qty, findex, listData) {
    qty = qty.toString();
    return (<TextInput
          style={styles.menuManagerInput}
          defaultValue={qty}
          onFocus={()=> this._setx()}
          value={(this.state.x) ? this.state.data[findex - 1].qty.toString() : null}
          editable= {(listData.disableAll) ? false : itemAvailable}
          keyboardType='numeric'
          selectTextOnFocus={true}
          onEndEditing={(event) =>
            this._onEnd(event, findex, listData.foodId, listData.qty, listData.itemAvailable)}

          // onChangeText={(value) => {
          //   this._toggleQty(findex, value);}
          // }
            />
    );
  }
  /*** Toggle Quanity ****/

  /*** Toggle Item Available ****/
  _toggleSwitchSuccess(findex, value, listData) {
    menuData[findex - 1].itemAvailable = value;
    this.setState({ data: menuData });
    this._changeData(listData.foodId, listData.qty, value);
  }

  _toggleSwitch(findex, value, listData) {
    this.popup.confirm({
        title: 'ChowMill',
        content: 'Do you want to proceed?',
        ok: {
          text: 'Yes',
          style: {
            color: 'red',
          },
          callback: () => {
            this._toggleSwitchSuccess(findex, value, listData);
          },
        },
      });
  }
  /*** Toggle Item Available ****/

  _renderRows(listData) {
    return (
      <Card>
        <CardItem style={styles.sideMenuPageCardItem}>
          <Grid>
            <Row>
              <Col size = {4}>
                <Text style={styles.sideMenuPageContentText}>
                  {listData.findex}. {listData.foodName}
                </Text>
              </Col>
              <Col size = {1}>
                <Label style={styles.sideMenuPageContentText}> QTY. </Label>
              </Col>
              <Col size = {1}>
                {this._inputQty(listData.itemAvailable, listData.qty, listData.findex, listData)}
              </Col>
              <Col size = {1}>
                <Switch
                  disabled = {this.state.disableAll}
                  onValueChange={(value) => this._toggleSwitch(listData.findex, value, listData)}
                  value={this.state.data[listData.findex - 1].itemAvailable} />
              </Col>
            </Row>
          </Grid>
        </CardItem>
      </Card>
    );
  }

  _onRefresh() {
    this.setState({ refreshing: true });
    this._loadInitialState().then(() => {
      this.setState({ refreshing: false });
    });
  }

  _refreshControl() {
    return (
      <RefreshControl
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh.bind(this)}
          tintColor="#D93F49"
          title="Loading..."
          titleColor="#D93F49"
          />
      );
  }

  render() {
    if (this.state.isLoading === true) {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Menu Manager</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content>
                      <ActivityIndicator
                        animating={this.state.isLoading} color='#D93F49' size='large'/>
                </Content>
              </Container>
          );
    } else if (this.state.noData === true) {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Menu Manager</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content refreshControl={this._refreshControl()}>
                  <Card>
                    <CardItem style={styles.sideMenuPageCardItem}>
                      <Text style={styles.purchaseProfileContentColor}>
                        No Data Found.</Text>
                    </CardItem>
                  </Card>
                </Content>
              </Container>
          );
    } else {
      return (
              <Container style={styles.themeBackgroundWhite}>
                <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                    <Left>
                      <Button transparent onPress={this._back.bind(this)}>
                          <Icon name='arrow-back' style={styles.themeBackButton}/>
                      </Button>
                    </Left>
                    <Body style={styles.themeHeaderTitleBody}>
                        <Title style={styles.sideMenuPageTitle}>Menu Manager</Title>
                    </Body>
                  <Right/>
                </Header>
                <Content refreshControl={this._refreshControl()}>
                      <ListView dataSource={this.ds.cloneWithRows(this.state.data)}
                        renderRow={(listData) =>
                          this._renderRows(listData)
                        } />
                </Content>
                <Popup ref={popup => this.popup = popup } isOverlayClickClose={false}/>
              </Container>
          );
    }
  }
}

export default MenuManager;
