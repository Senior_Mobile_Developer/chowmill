import React, { Component } from 'react';
import { Container, Content, Card, CardItem, Left,
  Body, Text, Header, Right, Title,
   Label, Button, Icon } from 'native-base';
import styles from '../style/style';
import { Actions } from 'react-native-router-flux';
import { WebView } from 'react-native';

let weburl = 'http://ec2-54-71-163-50.us-west-2.compute.amazonaws.com:3000/aboutus';

class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      webURL: weburl,
    };
  }

  _back() {
    Actions.pop();
  }

  render() {
    return (
            <Container style={styles.themeBackgroundWhite}>
              <Header style={styles.sideMenuPageStatusBar} noShadow={true} androidStatusBarColor='#D93F49'>
                  <Left>
                    <Button transparent onPress={this._back.bind(this)}>
                        <Icon name='arrow-back' style={styles.themeBackButton}/>
                    </Button>
                  </Left>
                  <Body style={styles.themeHeaderTitleBody}>
                      <Title style={styles.sideMenuPageTitle}>About Us</Title>
                  </Body>
                <Right/>
              </Header>
              <WebView
                    source = {{ uri: this.state.webURL }}
                    startInLoadingState = {true}
                    />
            </Container>
        );
  }
}

export default AboutUs;
